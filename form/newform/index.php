<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Form Permintaan Informasi</title>

    <!-- Bootstrap -->
   <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="js/fileinput.js" type="text/javascript"></script>
        <script src="js/fileinput_locale_fr.js" type="text/javascript"></script>
        <script src="js/fileinput_locale_es.js" type="text/javascript"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js" type="text/javascript"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>


  </head>
  <body>
    <div class="Permintaan-informasi">
      <div class="col-sm-offset-1 ">
      <h2>Form Permintaan Informasi</h2>
      </div> 

      <form data-toggle="validator" role="form" class="form-horizontal" action="input.php" method="POST">
        <div class="form-group">
        <label class="control-label col-xs-2">SKPD Tujuan :</label>
        <div class = "col-xs-3">
          <select class="form-control" name="kd_instansi">
        <option value=0 selected>- SKPD Tujuan -</option>
        <?php 
          include 'konek.php'; 
          $q = mysql_query("select * from ppid_instansi");
          while ($row1 = mysql_fetch_array($q)){
           echo "<option value = $row1[kd_instansi]>$row1[nm_instansi]</option>";
            }
          ?> </select>
        </div>
        </div>

        <div class="form-group">
          <label class="control-label col-xs-2" for="Nama">Nama : </label>
          <div class="col-xs-4">
                <input type="text" class="form-control" name="nama" placeholder="Nama Anda" required>
            </div>
        </div>

        <div class="form-group">
        <label class="control-label col-xs-2">Jenis Kelamin :</label>
        <div class = "col-xs-3">
          <select class="form-control" name="jenkel">
        <option value=0 selected>- Jenis Kelamin -</option>
        <?php 
          include 'konek.php';
         $q = mysql_query("select * from ppid_jenis_kelamin");
          while ($row1 = mysql_fetch_array($q)){
          echo "<option value = $row1[jenis_kelamin_id]>$row1[jenis_kelamin]</option>";
            }
          ?> </select>
        </div>
        </div>

         <div class="form-group">
          <label class="control-label col-xs-2" for="Usia">Usia : </label>
          <div class="col-xs-2">
                <input type="number" class="form-control" name="usia" placeholder="Usia Anda" required>
            </div>
        </div>

        <div class="form-group">
          <label class="control-label col-xs-2" for="NoKTP">No KTP : </label>
          <div class="col-xs-2">
                <input type="text" class="form-control" name="noktp" placeholder="No KTP" required>
            </div>

        </div>
         <div class="form-group">
          <label class="control-label col-xs-2" for="file">Scan KTP : </label>
           <div class="col-xs-4">
                    <input id="file-1" name ="file" type="file" data-overwrite-initial="false" data-min-file-count="1" required> *Format .jpg,.jpeg
            </div> 
         </div>
    

        <div class="form-group">
          <label class="control-label col-xs-2" for="Alamat">Alamat : </label>
          <div class="col-xs-3">
                <textarea class="form-control" name="alamat" placeholder="Alamat Lengkap" cols="40" required> </textarea>
            </div>
        </div>

         <div class="form-group">
          <label class="control-label col-xs-2" for="Telpone">Telephone : </label>
          <div class="col-xs-2">
                <input type="text" class="form-control" name="telpone" placeholder="No Telephone" required>
            </div>
        </div>

        <div class="form-group">
          <label class="control-label col-xs-2" for="Fax">Fax : </label>
          <div class="col-xs-2">
                <input type="text" class="form-control" name="fax" placeholder="No Fax" required>
            </div>
        </div>

        <div class="form-group">
          <label class="control-label col-xs-2" for="inputEmail">Email : </label>
          <div class="col-xs-4">
                <input type="email" class="form-control" name="email" placeholder="Email" data-error="Format Email Yang Anda Masukan Salah" required>
            </div>
            <div class="help-block with-errors"></div>
        </div>

         <div class="form-group">
          <label class="control-label col-xs-2" for="Informasi">Informasi yang Diminta : </label>
          <div class="col-xs-3">
                <textarea class="form-control" name="informasi" placeholder="Informasi yang Diminta" cols="40" required> </textarea>
            </div>
        </div>

         <div class="form-group">
          <label class="control-label col-xs-2" for="Alasan">Alasan Permintaan Informasi : </label>
          <div class="col-xs-3">
                <textarea class="form-control" name="alasan" placeholder="Alasan Permintaan" cols="40" required> </textarea>
            </div>
        </div>

         <div class="form-group">
          <label class="control-label col-xs-2" for="Cara">Cara Penyampaian : </label>
          <div class="col-xs-4">
                <input type="text" class="form-control" name="cara" placeholder="Cara Penyampaian" required>
            </div>
        </div>

        <div class="form-group">
        <label class="control-label col-xs-2">Tindak Lanjut :</label>
        <div class = "col-xs-3">
          <select class="form-control" name="tindaklanjut">
        <option value=0 selected>- Tindak Lanjut -</option>
        <?php 
          include 'konek.php';
         $q = mysql_query("select * from ppid_tindak_lanjut");
          while ($row1 = mysql_fetch_array($q)){
          echo "<option value = $row1[tindak_lanjut_id]>$row1[tindak_lanjut]</option>";
            }
          ?></select>
        </div>
        </div>

         <div class="form-group">
          <div class="col-sm-offset-2 col-sm-6">
          <div class="g-recaptcha" data-sitekey="6Lco4xUTAAAAADAcznM87pSkvYNulYBAbw-1IhxL"></div>
          </div>
        </div>

      <div class="form-group">
       <div class="col-sm-offset-2 col-sm-4">
         <input type="submit" value="Kirim" class="btn btn-sm btn-primary">
       </div>

      </form>

     
    </div>

  
  </body>
  <script>
    $('#file-fr').fileinput({
        language: 'fr',
        uploadUrl: '/img',
        allowedFileExtensions : ['jpg', 'png','gif'],
    });

    $('#file-es').fileinput({
        language: 'es',
        uploadUrl: '/img',
        allowedFileExtensions : ['jpg', 'png','gif'],
    });

    $("#file-0").fileinput({
        'allowedFileExtensions' : ['jpg', 'png','gif'],
    });

    $("#file-1").fileinput({
        uploadUrl: 'img', // you must set a valid URL here else you will get an error
        showUpload: false,
        allowedFileExtensions : ['jpg', 'jpeg', 'png','gif'],
        overwriteInitial: false,
        maxFileSize: 2000,
        maxFilesNum: 1,
        //allowedFileTypes: ['image', 'video', 'flash'],
        slugCallback: function(filename) {
           return filename.replace('(', '_').replace(']', '_');
        }
  });

    /*
    $(".file").on('fileselect', function(event, n, l) {
        alert('File Selected. Name: ' + l + ', Num: ' + n);
    });

    */
  $("#file-3").fileinput({
    showUpload: false,
    showCaption: false,
    browseClass: "btn btn-primary btn-lg",
    fileType: "jpg,jpeg,gif",
        previewFileIcon: "<i class='glyphicon glyphicon-king'> </i>"
  });

  $("#file-4").fileinput({
    uploadExtraData: {kvId: '10'}
  });
    $(".btn-warning").on('click', function() {
        if ($('#file-4').attr('disabled')) {
            $('#file-4').fileinput('enable');
        } else {
            $('#file-4').fileinput('disable');
        }
    });    
    $(".btn-info").on('click', function() {
        $('#file-4').fileinput('refresh', {previewClass:'bg-info'});
    });

    /*
    $('#file-4').on('fileselectnone', function() {
        alert('Huh! You selected no files.');
    });
    $('#file-4').on('filebrowse', function() {
        alert('File browse clicked for #file-4');
    });
    */
    $(document).ready(function() {
        $("#test-upload").fileinput({
            'showPreview' : false,
            'allowedFileExtensions' : ['jpg', 'png','gif'],
            'elErrorContainer': '#errorBlock'
        });

        /*
        $("#test-upload").on('fileloaded', function(event, file, previewId, index) {
            alert('i = ' + index + ', id = ' + previewId + ', file = ' + file.name);
        });
        */
    });
  </script>
</html>