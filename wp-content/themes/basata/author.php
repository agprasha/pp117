<?php get_header(); ?>
	<div id="content">
		<?php if ( have_posts() ): the_post(); ?>
		<div class="page-head">
			<h2 class="page-title">
				<?php printf( __( 'Author: %s', 'tie' ),  get_the_author() ); ?>
			</h2>
			
			<div class="author-bio">
				<div class="author-avatar">
					<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'tie_author_bio_avatar_size', 75 ) ); ?>
				</div><!-- #author-avatar -->
				
				<div class="author-description">
					<?php the_author_meta( 'description' ); ?>
				</div><!-- #author-description -->
				<div class="social-icons-widget author-social">
					<?php if ( get_the_author_meta( 'twitter' ) ) : ?>
					<a class="ttip" href="http://twitter.com/<?php the_author_meta( 'twitter' ); ?>" title="<?php the_author_meta( 'display_name' ); ?><?php _e( '  on Twitter', 'tie' ); ?>"><i class="tieicon-twitter"></i></a>
					<?php endif ?>	
					<?php if ( get_the_author_meta( 'facebook' ) ) : ?>
					<a class="ttip" href="<?php the_author_meta( 'facebook' ); ?>" title="<?php the_author_meta( 'display_name' ); ?> <?php _e( '  on Facebook', 'tie' ); ?>"><i class="tieicon-facebook"></i></a>
					<?php endif ?>
					<?php if ( get_the_author_meta( 'google' ) ) : ?>
					<a class="ttip" href="<?php the_author_meta( 'google' ); ?>" title="<?php the_author_meta( 'display_name' ); ?> <?php _e( '  on Google+', 'tie' ); ?>"><i class="tieicon-gplus"></i></a>
					<?php endif ?>	
					<?php if ( get_the_author_meta( 'linkedin' ) ) : ?>
					<a class="ttip" href="<?php the_author_meta( 'linkedin' ); ?>" title="<?php the_author_meta( 'display_name' ); ?> <?php _e( '  on Linkedin', 'tie' ); ?>"><i class="tieicon-linkedin"></i></a>
					<?php endif ?>				
					<?php if ( get_the_author_meta( 'flickr' ) ) : ?>
					<a class="ttip" href="<?php the_author_meta( 'flickr' ); ?>" title="<?php the_author_meta( 'display_name' ); ?><?php _e( '  on Flickr', 'tie' ); ?>"><i class="tieicon-flickr"></i></a>
					<?php endif ?>	
					<?php if ( get_the_author_meta( 'youtube' ) ) : ?>
					<a class="ttip" href="<?php the_author_meta( 'youtube' ); ?>" title="<?php the_author_meta( 'display_name' ); ?><?php _e( '  on YouTube', 'tie' ); ?>"><i class="tieicon-youtube"></i></a>
					<?php endif ?>
					<?php if ( get_the_author_meta( 'pinterest' ) ) : ?>
					<a class="ttip" href="<?php the_author_meta( 'pinterest' ); ?>" title="<?php the_author_meta( 'display_name' ); ?><?php _e( '  on Pinterest', 'tie' ); ?>"><i class="tieicon-pinterest-circled"></i></a>
					<?php endif ?>
				</div>
			</div>
		</div>
		<?php endif; ?>

		<?php
		rewind_posts();
		get_template_part( 'loop', 'author' );	?>
		<?php if ($wp_query->max_num_pages > 1) tie_pagenavi(); ?>
	</div> <!-- .content -->
<?php get_footer(); ?>