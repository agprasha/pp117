<?php get_header(); ?>
	<div id="content">
		<?php $category_id = get_query_var('cat') ; ?>
		<div class="page-head">
			<h2 class="page-title">
				<?php printf( __( '%s', 'tie' ), '<span>' . single_cat_title( '', false ) . '</span>' );	?>
			</h2>

			<?php
				$category_description = category_description();
				if ( ! empty( $category_description ) )
				echo '<div class="clear"></div><div class="archive-meta">' . $category_description . '</div>';
			?>
		</div>
		
		<?php get_template_part( 'loop', 'category' );	?>
		<?php if ($wp_query->max_num_pages > 1) tie_pagenavi(); ?>
	</div> <!-- .content -->
<?php get_footer(); ?>