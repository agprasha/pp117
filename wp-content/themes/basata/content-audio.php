<?php
 	$get_meta = get_post_custom($post->ID);
	$soundcloud = $get_meta["tie_audio_soundcloud"][0];
	if( !empty( $soundcloud ) ){
		echo tie_soundcloud( $soundcloud );
	}else{ ?>
		<div class="post-media<?php if ( has_post_thumbnail() ) echo' has-audio-cover'; ?>">
			<?php
			if ( has_post_thumbnail() ){ ?>
			<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'tie' ), the_title_attribute( 'echo=0' ) ); ?>">
			<?php the_post_thumbnail( 'tie-post' ); ?>
			</a>
			<?php } ?>
		<?php
			$mp3 = $get_meta["tie_audio_mp3"][0];
			$m4a = $get_meta["tie_audio_m4a"][0];
			$oga = $get_meta["tie_audio_oga"][0];
			echo '<div class="post-audio-player">'.do_shortcode('[audio mp3="'.$mp3.'" ogg="'.$oga.'" m4a="'.$m4a.'"]').'</div>';
		?>
		</div>
<?php
	}
?>
<?php 
	if( !is_singular() ) { ?>
		<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'tie' ), the_title_attribute( 'echo=0' ) ); ?>"><?php the_title(); ?></a></h2>
	<?php } else { ?>
		<h1 class="entry-title"><?php the_title(); ?></h1>
	<?php } ?>
	<?php get_template_part( 'includes/post-meta' ); // Get Post Meta template ?>	
<?php if( is_singular() ) { ?>
	<div class="entry">
		<?php the_content( __( 'Read More &raquo;', 'tie' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'tie' ), 'after' => '</div>' ) ); ?>
	</div>
<?php } ?>