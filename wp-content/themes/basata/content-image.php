<?php if( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { ?>
<div class="post-media">
	<a href="<?php echo tie_thumb_src( 'full' ) ?>" rel="prettyPhoto"><?php the_post_thumbnail( 'tie-post' ) ; ?><i class="tieicon-search"></i></a>
</div>
<?php } ?>

<?php if( is_singular() ) { ?>
	<h1 class="entry-title"><?php the_title(); ?></h1>
	<?php get_template_part( 'includes/post-meta' ); // Get Post Meta template ?>	
	<div class="entry">
		<?php the_content( __( 'Read More &raquo;', 'tie' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'tie' ), 'after' => '</div>' ) ); ?>
	</div>
<?php } ?>