<div class="entry-link">
<?php $link_url = get_post_meta($post->ID, 'tie_link_url', true);  ?>
<?php $link_desc = get_post_meta($post->ID, 'tie_link_desc', true);  ?>
<?php if ( !is_singular() ) { ?>
	<h2 class="entry-title"><a href="<?php echo $link_url; ?>" target="_blank"><?php the_title(); ?></a></h2>
<?php } else { ?>
    <h1 class="entry-title"><a href="<?php echo $link_url; ?>" target="_blank"><?php the_title(); ?></a></h1>
<?php } ?>

<?php if( !empty($link_desc) ): ?>
	<span class="link-url">
		<?php echo $link_desc; ?>
	</span>
<?php endif; ?>
</div>

<?php if( is_singular() ) { ?>
	<?php get_template_part( 'includes/post-meta' ); // Get Post Meta template ?>	
	<div class="entry">
		<?php the_content( __( 'Read More &raquo;', 'tie' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'tie' ), 'after' => '</div>' ) ); ?>
	</div>
<?php } ?>
