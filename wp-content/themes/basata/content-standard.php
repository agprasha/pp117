<?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() ) : ?>			
<div class="post-media standard-img">
	<a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'tie' ), the_title_attribute( 'echo=0' ) ); ?>"><?php the_post_thumbnail( 'tie-post' ); ?></a>
</div>
<?php endif; ?>
<?php 
if( !is_singular() ) { ?>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'tie' ), the_title_attribute( 'echo=0' ) ); ?>"><?php the_title(); ?></a></h2>
	<?php get_template_part( 'includes/post-meta' ); // Get Post Meta template ?>	
	<div class="entry">
		<?php the_content( __( 'Read More &raquo;', 'tie' ) ); ?>
	</div>
<?php } else { ?>
    <h1 class="entry-title"><?php the_title(); ?></h1>
	<?php get_template_part( 'includes/post-meta' ); // Get Post Meta template ?>	
	<div class="entry">
		<?php the_content( __( 'Read More &raquo;', 'tie' ) ); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'tie' ), 'after' => '</div>' ) ); ?>
	</div>
<?php } ?>