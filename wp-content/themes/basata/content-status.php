<?php
$status_facebook = get_post_meta($post->ID, 'tie_status_facebook', true);
$status_twitter = get_post_meta($post->ID, 'tie_status_twitter', true);
if( !empty( $status_facebook ) || !empty( $status_twitter ) ):	?>
<div class="embed-conatiner">
<?php if( !empty( $status_facebook ) ) : ?>
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
	<div class="fb-post" data-href="<?php echo $status_facebook ?>"></div>
<?php elseif( !empty( $status_twitter ) ) : ?>
	<blockquote class="twitter-tweet"><a href="<?php echo $status_twitter ?>"></a></blockquote>
	<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
<?php endif; ?>
</div>
<?php endif; ?>
<div class="entry">
	<?php the_content( __( 'Read More &raquo;', 'tie' ) ); ?>
</div>