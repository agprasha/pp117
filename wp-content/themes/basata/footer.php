		<div class="clear"></div>
		<div class="copyrights"><?php echo htmlspecialchars_decode(tie_get_option( 'footer_one' )) ?></div>
	</div><!-- .container /-->
</div>
<?php if( tie_get_option('footer_top') ): ?>
	<div id="topcontrol" class="tieicon-up" title="<?php _e('Scroll To Top' , 'tie'); ?>"></div>
<?php endif; ?>
<?php wp_footer(); ?>
</body>
</html>