<?php
/*-----------------------------------------------------------------------------------*/
# Register main Scripts and Styles
/*-----------------------------------------------------------------------------------*/
add_action( 'wp_enqueue_scripts', 'tie_register' ); 
function tie_register() {
	## Register Main style.css file
	wp_register_style( 'tie-style', get_stylesheet_uri() , array(), '', 'all' );
	wp_enqueue_style( 'tie-style' );
	
    wp_register_script( 'tie-scripts', get_template_directory_uri() . '/js/tie-scripts.js', array( 'jquery' ), false , true );  
	wp_enqueue_script( 'tie-scripts' );

	## Enquee Default Fonts
	$protocol = is_ssl() ? 'https' : 'http';
	wp_enqueue_style( "Lato" , "$protocol://fonts.googleapis.com/css?family=Lato:300,900");
	wp_enqueue_style( "Source+Sans+Pro" , "$protocol://fonts.googleapis.com/css?family=Source+Sans+Pro%3A200%2C400");
	
	## For facebook & Google + share
	if(  is_page() || is_single() )	tie_og_image();  ?>
<?php
}

/*-----------------------------------------------------------------------------------*/
# Enqueue Fonts From Google
/*-----------------------------------------------------------------------------------*/
function tie_enqueue_font ( $got_font) {
	if ($got_font) {
	
		$char_set = '';
		if( tie_get_option('typography_latin_extended') || tie_get_option('typography_cyrillic') ||
		tie_get_option('typography_cyrillic_extended') || tie_get_option('typography_greek') ||
		tie_get_option('typography_greek_extended') ){
		
			$char_set = '&subset=latin';
			if( tie_get_option('typography_latin_extended') ) 
				$char_set .= ',latin-ext';
			if( tie_get_option('typography_cyrillic') )
				$char_set .= ',cyrillic';
			if( tie_get_option('typography_cyrillic_extended') )
				$char_set .= ',cyrillic-ext';
			if( tie_get_option('typography_greek') )
				$char_set .= ',greek';
			if( tie_get_option('typography_greek_extended') )
				$char_set .= ',greek-ext';
			if( tie_get_option('typography_khmer') )
				$char_set .= ',khmer';
			if( tie_get_option('typography_vietnamese') )
				$char_set .= ',vietnamese';
		}
		
		$font_pieces = explode(":", $got_font);
		
		$font_name = $font_pieces[0];
		$font_type = $font_pieces[1];
		
		if( $font_type == 'non-google' ){
		
			// Do Nothing :)
			
		}elseif( $font_type == 'early-google'){
			$font_name = str_replace (" ","", $font_pieces[0] );
			$protocol = is_ssl() ? 'https' : 'http';
			wp_enqueue_style( $font_name , $protocol.'://fonts.googleapis.com/earlyaccess/'.$font_name);
			
		}else{
			$font_name = str_replace (" ","+", $font_pieces[0] );
			$font_variants = str_replace ("|",",", $font_pieces[1] );
			$protocol = is_ssl() ? 'https' : 'http';
			wp_enqueue_style( $font_name , $protocol.'://fonts.googleapis.com/css?family='.$font_name . ':' . $font_variants.$char_set );
		}
	}
}


/*-----------------------------------------------------------------------------------*/
# Get Font Name
/*-----------------------------------------------------------------------------------*/
function tie_get_font ( $got_font ) {
	if ($got_font) {
		$font_pieces = explode(":", $got_font);
		$font_name = $font_pieces[0];
		$font_name = str_replace('&quot;' , '"' , $font_pieces[0] );
		if (strpos($font_name, ',') !== false) 
			return $font_name;
		else
			return "'".$font_name."'";
	}
}


/*-----------------------------------------------------------------------------------*/
# Typography Elements Array
/*-----------------------------------------------------------------------------------*/
$custom_typography = array(
	"body"								=>		"typography_general",
	"#main-nav, #main-nav ul li a"		=>		"typography_main_nav",
	".entry-title, .entry-title a"		=> 		"typography_post_title",
	"p.post-meta, p.post-meta a  "		=> 		"typography_post_meta",
	".entry"							=> 		"typography_post_entry",
	"h4.widget-top, h4.widget-top a"	=> 		"typography_widgets_title",
	".entry h1"				=> 		"typography_post_h1",
	".entry h2"				=> 		"typography_post_h2",
	".entry h3"				=> 		"typography_post_h3",
	".entry h4"				=> 		"typography_post_h4",
	".entry h5"				=> 		"typography_post_h5",
	".entry h6"				=> 		"typography_post_h6"
);
	
	
/*-----------------------------------------------------------------------------------*/
# Get Custom Typography
/*-----------------------------------------------------------------------------------*/
add_action('wp_enqueue_scripts', 'tie_typography');
function tie_typography(){
	global $custom_typography;

	foreach( $custom_typography as $selector => $value){
		$option = tie_get_option( $value );
		
		if( !empty($option['font']))
			tie_enqueue_font( $option['font'] ) ;
	}
}


/*-----------------------------------------------------------------------------------*/
# Tie Wp Head
/*-----------------------------------------------------------------------------------*/
add_action('wp_head', 'tie_wp_head');
function tie_wp_head() {
	global $custom_typography;
	$prettyPhoto = tie_get_option( 'lightbox_style' );
	if( empty($prettyPhoto) ) $prettyPhoto = 'light_rounded'; ?>
	
<script type='text/javascript'>
/* <![CDATA[ */
var tie = {"prettyPhoto" : "<?php echo $prettyPhoto ?>" };
/* ]]> */
</script>
<?php
	if( is_singular() ){
		global $post;
		$tie_post_bg = get_post_meta($post->ID, 'tie_post_bg', true);
		if( empty($tie_post_bg) && is_single() ){
			$category = get_the_category($post->ID);
			$cat_id = $category[0]->cat_ID;
			$cat_option = get_option('tie_cat_'.$cat_id);
			if( !empty($cat_option['tie_post_bg']) )
				$tie_post_bg = $cat_option['tie_post_bg'];
		}
	}elseif( is_category() ){
		$cat_id = get_query_var('cat');
		$cat_option = get_option('tie_cat_'.$cat_id);
		if( !empty($cat_option['tie_post_bg']) )
			$tie_post_bg = $cat_option['tie_post_bg'];
	}
?>
<!--[if lte IE 9]>
<script src="<?php echo get_template_directory_uri() ?>/js/ie.js"></script>
<![endif]-->
<!--[if IE 8]>
<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri() ?>/css/ie8.css" />
<![endif]-->
<?php echo "\n"; ?>
<style type="text/css" media="screen"> 
<?php
foreach( $custom_typography as $selector => $value){
$option = tie_get_option( $value );
if( $option['font'] || $option['color'] || $option['size'] || $option['weight'] || $option['style'] ):
echo "\n".$selector."{\n"; ?>
<?php if($option['font'] )
	echo "	font-family: ". tie_get_font( $option['font']  ).";\n"?>
<?php if( !empty( $option['color'] ) )
	echo "	color :". $option['color'].";\n"?>
<?php if( !empty( $option['size'] ) )
	echo "	font-size : ".$option['size']."px;\n"?>
<?php if( !empty( $option['weight'] ) )
	echo "	font-weight: ".$option['weight'].";\n"?>
<?php if( !empty( $option['style'] ) )
	echo "	font-style: ". $option['style'].";\n"?>
}
<?php endif;
} ?>
<?php if( tie_get_option( 'theme_color' ) ) tie_theme_color( tie_get_option( 'theme_color' ) ); ?>
<?php
for( $i=1; $i<11; $i++){
	if( tie_get_option( "custom_color_$i" ) ){
		tie_theme_color( tie_get_option( "custom_color_$i" ) , ".custom_color_$i" );
	}
}
?>							
<?php if( tie_get_option( 'links_color' ) || tie_get_option( 'links_decoration' )  ): ?>
a {
	<?php if( tie_get_option( 'links_color' ) ) echo 'color: '.tie_get_option( 'links_color' ).';'; ?>
	<?php if( tie_get_option( 'links_decoration' ) ) echo 'text-decoration: '.tie_get_option( 'links_decoration' ).';'; ?>
}
<?php endif; ?>
<?php if( tie_get_option( 'links_color_hover' ) || tie_get_option( 'links_decoration_hover' )  ): ?>
a:hover {
	<?php if( tie_get_option( 'links_color_hover' ) ) echo 'color: '.tie_get_option( 'links_color_hover' ).';'; ?>
	<?php if( tie_get_option( 'links_decoration_hover' ) ) echo 'text-decoration: '.tie_get_option( 'links_decoration_hover' ).';'; ?>
}
<?php endif; ?>

<?php
if( !empty( $tie_post_bg )): ?>
	#sidebar .cover-img {background-image: url('<?php echo $tie_post_bg ?>') ;}<?php echo "\n"; ?>
<?php else:
	$main_sidebar_bg = tie_get_option( 'main_sidebar_bg' );
	if( !empty($main_sidebar_bg ) ): ?>
		#sidebar .cover-img {background-image: url('<?php echo $main_sidebar_bg ?>') ;}<?php echo "\n"; ?>
	<?php endif; ?>
<?php endif; ?>
<?php
$nav_bg_opacity = tie_get_option( 'nav_bg_opacity' );
if( $nav_bg_opacity ): ?>
#main-nav ul li a { background: rgba(255,255,255, <?php echo ( $nav_bg_opacity/100 ) ?>); }<?php echo "\n"; ?>
<?php endif; ?>
<?php if( tie_get_option( 'post_links_color' ) || tie_get_option( 'post_links_decoration' )  ): ?>
.post .entry a {
	<?php if( tie_get_option( 'post_links_color' ) ) echo 'color: '.tie_get_option( 'post_links_color' ).';'; ?>
	<?php if( tie_get_option( 'post_links_decoration' ) ) echo 'text-decoration: '.tie_get_option( 'post_links_decoration' ).';'; ?>
}
<?php endif; ?>
<?php if( tie_get_option( 'post_links_color_hover' ) || tie_get_option( 'post_links_decoration_hover' )  ): ?>
.post .entry a:hover {
	<?php if( tie_get_option( 'post_links_color_hover' ) ) echo 'color: '.tie_get_option( 'post_links_color_hover' ).';'; ?>
	<?php if( tie_get_option( 'post_links_decoration_hover' ) ) echo 'text-decoration: '.tie_get_option( 'post_links_decoration_hover' ).';'; ?>
}
<?php endif; ?>
<?php $css_code =  str_replace("<pre>", "", htmlspecialchars_decode( tie_get_option('css')) ); 
echo $css_code = str_replace("</pre>", "", $css_code )  , "\n";?>
<?php if( tie_get_option('css_tablets') ) : ?>
@media only screen and (max-width: 985px) and (min-width: 768px){
<?php $css_code1 =  str_replace("<pre>", "", htmlspecialchars_decode( tie_get_option('css_tablets')) ); 
echo $css_code1 = str_replace("</pre>", "", $css_code1 )  , "\n";?>
}
<?php endif; ?>
<?php if( tie_get_option('css_wide_phones') ) : ?>
@media only screen and (max-width: 767px) and (min-width: 480px){
<?php $css_code2 =  str_replace("<pre>", "", htmlspecialchars_decode( tie_get_option('css_wide_phones')) ); 
echo $css_code2 = str_replace("</pre>", "", $css_code2 )  , "\n";?>
}
<?php endif; ?>
<?php if( tie_get_option('css_phones') ) : ?>
@media only screen and (max-width: 479px) and (min-width: 320px){
<?php $css_code3 =  str_replace("<pre>", "", htmlspecialchars_decode( tie_get_option('css_phones')) ); 
echo $css_code3 = str_replace("</pre>", "", $css_code3 )  , "\n";?>
}
<?php endif; ?>
</style> 

<?php
echo htmlspecialchars_decode( tie_get_option('header_code') ) , "\n";
}


/*-----------------------------------------------------------------------------------*/
# Theme Colors Function
/*-----------------------------------------------------------------------------------*/
function tie_theme_color( $color , $prefix = '' ){ ?>
	<?php echo $prefix ?>.item-list {border-top-color: <?php echo $color; ?>;}
	<?php echo $prefix ?>.item-list .post-format-icon, <?php echo $prefix ?> .chat-speaker-2 .chat-text, <?php echo $prefix ?> .entry a.more-link, <?php echo $prefix ?> .pagination a:hover , <?php echo $prefix ?> .pagination span.current,
	<?php echo $prefix ?> input[type=submit]:hover, <?php echo $prefix ?> .form-submit #submit:hover, <?php echo $prefix ?> #login-form .login-button:hover , <?php echo $prefix ?> .widget-feedburner .feedburner-subscribe:hover , <?php echo $prefix ?> .password-protected input[type="submit"]:hover, <?php echo $prefix ?> .no-thumbnail a:hover,
	<?php echo $prefix ?> #main-nav ul li.horiz-menu ul.sub-menu{
		background-color:<?php echo $color; ?> ;
	}
	<?php echo $prefix ?> a:hover, <?php echo $prefix ?> .chat-speaker-2 cite{color:<?php echo $color; ?>; }
	<?php echo $prefix ?> .chat-speaker-2.chat-row .chat-text:before {border-right-color:<?php echo $color; ?> ; }
	<?php echo $prefix ?> .chat-speaker-2.chat-row:nth-child(2n+2) .chat-text:before {border-left-color:<?php echo $color; ?>; }
	<?php echo $prefix ?> .post-social a:hover [class^="tieicon-"]:before, <?php echo $prefix ?> .post-social a:hover [class*=" tieicon-"]:before{
		color: <?php echo $color; ?> ;
		border-color: <?php echo $color; ?> ;
	}
<?php
}
?>