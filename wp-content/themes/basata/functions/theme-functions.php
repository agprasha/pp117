<?php

/*-----------------------------------------------------------------------------------*/
# Get Theme Options
/*-----------------------------------------------------------------------------------*/
function tie_get_option( $name ) {
	$get_options = get_option( 'tie_options' );
	
	if( !empty( $get_options[$name] ))
		return $get_options[$name];
		
	return false ;
}

/*-----------------------------------------------------------------------------------*/
# Setup Theme
/*-----------------------------------------------------------------------------------*/
add_action( 'after_setup_theme', 'tie_setup' );
function tie_setup() {
	global $default_data;
	
	if ( function_exists( 'add_theme_support' ) ){
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'post-formats', array('aside', 'gallery', 'image', 'link', 'quote', 'video', 'audio', 'chat', 'status' ) );
	}
	load_theme_textdomain( 'tie', get_template_directory() . '/languages' );

	register_nav_menus( array(
		'primary' => __( 'Primary Navigation', 'tie' )
	) );
	
}


/*-----------------------------------------------------------------------------------*/
# Post Thumbinals
/*-----------------------------------------------------------------------------------*/
if ( function_exists( 'add_image_size' ) ){
	add_image_size( 'tie-small', 70, 70, true );
	add_image_size( 'tie-post', 770, 500 , true );
}

/*-----------------------------------------------------------------------------------*/
# Get Post Video  
/*-----------------------------------------------------------------------------------*/
function tie_vedio(){
	global $post;
	
	$get_meta = get_post_custom($post->ID);
	
	if( isset( $get_meta["tie_video_url"][0] ) && !empty( $get_meta["tie_video_url"][0] ) ){
		$video_url = $get_meta["tie_video_url"][0];
		$video_link = @parse_url($video_url);
		if ( $video_link['host'] == 'www.youtube.com' || $video_link['host']  == 'youtube.com' ) {
			parse_str( @parse_url( $video_url, PHP_URL_QUERY ), $my_array_of_vars );
			$video =  $my_array_of_vars['v'] ;
			$video_code ='<iframe width="600" height="325" src="http://www.youtube.com/embed/'.$video.'?rel=0&wmode=opaque" frameborder="0" allowfullscreen></iframe>';
		}
		elseif( $video_link['host'] == 'www.vimeo.com' || $video_link['host']  == 'vimeo.com' ){
			$video = (int) substr(@parse_url($video_url, PHP_URL_PATH), 1);
			$video_code='<iframe width="600" height="325" src="http://player.vimeo.com/video/'.$video.'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
		}
		elseif( $video_link['host'] == 'www.youtu.be' || $video_link['host']  == 'youtu.be' ){
			$video = substr(@parse_url($video_url, PHP_URL_PATH), 1);
			$video_code ='<iframe width="600" height="325" src="http://www.youtube.com/embed/'.$video.'?rel=0" frameborder="0" allowfullscreen></iframe>';
		}elseif( $video_link['host'] == 'www.dailymotion.com' || $video_link['host']  == 'dailymotion.com' ){
			$video = substr(@parse_url($video_url, PHP_URL_PATH), 7);
			$video_id = strtok($video, '_');
			$video_code='<iframe frameborder="0" width="600" height="325" src="http://www.dailymotion.com/embed/video/'.$video_id.'"></iframe>';
		}
	}
	elseif( isset( $get_meta["tie_embed_code"][0] ) ){
		$embed_code = $get_meta["tie_embed_code"][0];
		$video_code = htmlspecialchars_decode( $embed_code);
	}
	if( isset($video_code) ) echo $video_code;
}


/*-----------------------------------------------------------------------------------*/
# Get Post Gallery  
/*-----------------------------------------------------------------------------------*/
function tie_gallery(){
	global $post;
		
	$custom = get_post_custom($post->ID);
	$images = unserialize( $custom["post_gallery"][0] );
	$gallery_type = $custom["tie_gallery_style"][0] ;
	$number = count($images);
		
if( $images ):
	if( $gallery_type == 'grid' ):
		$gallery_class = '';
		$count = 0;
		if	  ( $number == 2 ) $gallery_class = ' grid-2';
		elseif( $number % 2 == 0 ) $gallery_class = ' grid-even';
		elseif( $number % 2 != 0 ) $gallery_class = ' grid-odd';
		?>
		<div class="gallery-grid<?php echo $gallery_class; ?>">
			<ul>
			<?php foreach( $images as $single_image ): $count++;
				$image = wp_get_attachment_image_src( $single_image['id'] , 'tie-post'  );
				$large_image = wp_get_attachment_image_src( $single_image['id'] , 'full'  );
				if( $count >= ( ( $number + 1 ) - 3 ) ) $last_items = ' last-item-class';
				else $last_items = '';

				if( $count == $number  ) $last_item = ' grid-last-item';
				else $last_item = '';
			?>			  
			<li class="grid-item-<?php echo $count.$last_items.$last_item; ?>">
				<a href="<?php echo $large_image[0] ?>" rel="prettyPhoto[gallery_<?php echo $post->ID ?>]">
					<img src="<?php echo $image[0] ?>" alt="" />
					<i class="tieicon-search"></i>
				</a>
			</li>
			<?php endforeach; ?>
			</ul>
		</div>
		<div class="clear"></div>
<?php
	else: ?>
		<div class="flexslider">
			<ul class="slides">
			<?php foreach( $images as $single_image ):
				$image = wp_get_attachment_image_src( $single_image['id'] , 'tie-post'  );
				$large_image = wp_get_attachment_image_src( $single_image['id'] , 'full'  );
			?>			  
			<li>
				<a href="<?php echo $large_image[0] ?>" rel="prettyPhoto[gallery_<?php echo $post->ID ?>]">
					<img src="<?php echo $image[0] ?>" alt="" />
					<i class="tieicon-search"></i>
				</a>
			</li>
			<?php endforeach; ?>
			</ul>
		</div>
		<script>
		jQuery(document).ready(function() {
		  jQuery('.flexslider').flexslider({
				animation: 'slide',
				slideshowSpeed: 7000,
				animationSpeed: 600,
				randomize: false,
				controlNav: true,
				pauseOnHover: true,
				smoothHeight : true,
				prevText: '<i class="tieicon-left-arrow"></i>',
				nextText: '<i class="tieicon-right-arrow"></i>', 
			});
		});
		</script>
<?php	
	endif;
endif;
}

	
/*-----------------------------------------------------------------------------------*/
# Custom Dashboard login page logo
/*-----------------------------------------------------------------------------------*/
function tie_login_logo(){
	if( tie_get_option('dashboard_logo') )
    echo '<style  type="text/css"> .login h1 a {  background-image:url('.tie_get_option('dashboard_logo').')  !important; background-size: 274px 63px; width: 326px; height: 67px; } </style>';  
}  
add_action('login_head',  'tie_login_logo'); 


/*-----------------------------------------------------------------------------------*/
# Custom Gravatar
/*-----------------------------------------------------------------------------------*/
function tie_custom_gravatar ($avatar) {
	$tie_gravatar = tie_get_option( 'gravatar' );
	if($tie_gravatar){
		$custom_avatar = tie_get_option( 'gravatar' );
		$avatar[$custom_avatar] = "Custom Gravatar";
	}
	return $avatar;
}
add_filter( 'avatar_defaults', 'tie_custom_gravatar' ); 


/*-----------------------------------------------------------------------------------*/
# Custom Favicon
/*-----------------------------------------------------------------------------------*/
function tie_favicon() {
	$default_favicon = get_template_directory_uri()."/favicon.ico";
	$custom_favicon = tie_get_option('favicon');
	$favicon = (empty($custom_favicon)) ? $default_favicon : $custom_favicon;
	echo '<link rel="shortcut icon" href="'.$favicon.'" title="Favicon" />';
}
add_action('wp_head', 'tie_favicon');


/*-----------------------------------------------------------------------------------*/
#Author Box
/*-----------------------------------------------------------------------------------*/
function tie_author_box($avatar = true , $social = true ){
	if( $avatar ) : ?>
	<div class="author-avatar">
		<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'MFW_author_bio_avatar_size', 70 ) ); ?>
	</div><!-- #author-avatar -->
	<?php endif; ?>
		<div class="author-description">
			<?php the_author_meta( 'description' ); ?>
		</div><!-- #author-description -->
	<?php  if( $social ) :	?>	
		<div class="social-icons-widget author-social">
			<?php if ( get_the_author_meta( 'twitter' ) ) : ?>
			<a class="ttip" href="http://twitter.com/<?php the_author_meta( 'twitter' ); ?>" title="<?php the_author_meta( 'display_name' ); ?><?php _e( '  on Twitter', 'tie' ); ?>"><i class="tieicon-twitter"></i></a>
			<?php endif ?>	
			<?php if ( get_the_author_meta( 'facebook' ) ) : ?>
			<a class="ttip" href="<?php the_author_meta( 'facebook' ); ?>" title="<?php the_author_meta( 'display_name' ); ?> <?php _e( '  on Facebook', 'tie' ); ?>"><i class="tieicon-facebook"></i></a>
			<?php endif ?>
			<?php if ( get_the_author_meta( 'google' ) ) : ?>
			<a class="ttip" href="<?php the_author_meta( 'google' ); ?>" title="<?php the_author_meta( 'display_name' ); ?> <?php _e( '  on Google+', 'tie' ); ?>"><i class="tieicon-gplus"></i></a>
			<?php endif ?>	
			<?php if ( get_the_author_meta( 'linkedin' ) ) : ?>
			<a class="ttip" href="<?php the_author_meta( 'linkedin' ); ?>" title="<?php the_author_meta( 'display_name' ); ?> <?php _e( '  on Linkedin', 'tie' ); ?>"><i class="tieicon-linkedin"></i></a>
			<?php endif ?>				
			<?php if ( get_the_author_meta( 'flickr' ) ) : ?>
			<a class="ttip" href="<?php the_author_meta( 'flickr' ); ?>" title="<?php the_author_meta( 'display_name' ); ?><?php _e( '  on Flickr', 'tie' ); ?>"><i class="tieicon-flickr"></i></a>
			<?php endif ?>	
			<?php if ( get_the_author_meta( 'youtube' ) ) : ?>
			<a class="ttip" href="<?php the_author_meta( 'youtube' ); ?>" title="<?php the_author_meta( 'display_name' ); ?><?php _e( '  on YouTube', 'tie' ); ?>"><i class="tieicon-youtube"></i></a>
			<?php endif ?>
			<?php if ( get_the_author_meta( 'pinterest' ) ) : ?>
			<a class="ttip" href="<?php the_author_meta( 'pinterest' ); ?>" title="<?php the_author_meta( 'display_name' ); ?><?php _e( '  on Pinterest', 'tie' ); ?>"><i class="tieicon-pinterest-circled"></i></a>
			<?php endif ?>
		</div>
	<?php endif; ?>
	<div class="clear"></div>
	<?php
}


/*-----------------------------------------------------------------------------------*/
# Get Social Counter
/*-----------------------------------------------------------------------------------*/
function tie_remote_get( $url ) {
	$request = wp_remote_retrieve_body( wp_remote_get( $url , array( 'timeout' => 18 , 'sslverify' => false ) ) );
	return $request;
}

function tie_followers_count() {
	$twitter_username 		= tie_get_option('twitter_username');
	$twitter['page_url'] = 'http://www.twitter.com/'.$twitter_username;
	$twitter['followers_count'] = get_transient('twitter_count');
	if( empty( $twitter['followers_count']) ){
		try {
		
			$data = @json_decode(tie_remote_get("https://twitter.com/users/$twitter_username.json") , true);
			$twitter['followers_count'] = (int) $data['followers_count'];	
			
			$consumerKey 			= tie_get_option('twitter_consumer_key');
			$consumerSecret			= tie_get_option('twitter_consumer_secret');

			$token = get_option('tie_TwitterToken');
		 
			// getting new auth bearer only if we don't have one
			if(!$token) {
				// preparing credentials
				$credentials = $consumerKey . ':' . $consumerSecret;
				$toSend = base64_encode($credentials);
		 
				// http post arguments
				$args = array(
					'method' => 'POST',
					'httpversion' => '1.1',
					'blocking' => true,
					'headers' => array(
						'Authorization' => 'Basic ' . $toSend,
						'Content-Type' => 'application/x-www-form-urlencoded;charset=UTF-8'
					),
					'body' => array( 'grant_type' => 'client_credentials' )
				);
		 
				add_filter('https_ssl_verify', '__return_false');
				$response = wp_remote_post('https://api.twitter.com/oauth2/token', $args);
		 
				$keys = json_decode(wp_remote_retrieve_body($response));
		 
				if($keys) {
					// saving token to wp_options table
					update_option('tie_TwitterToken', $keys->access_token);
					$token = $keys->access_token;
				}
			}
			
			// we have bearer token wether we obtained it from API or from options
			$args = array(
				'httpversion' => '1.1',
				'blocking' => true,
				'headers' => array(
					'Authorization' => "Bearer $token"
				)
			);
		 
			add_filter('https_ssl_verify', '__return_false');
			$api_url = "https://api.twitter.com/1.1/users/show.json?screen_name=$twitter_username";
			$response = wp_remote_get($api_url, $args);
		 
			if (!is_wp_error($response)) {
				$followers = json_decode(wp_remote_retrieve_body($response));
				$twitter['followers_count'] = $followers->followers_count;
			} 
			
		} catch (Exception $e) {
			$twitter['followers_count'] = 0;
		}
		if( !empty( $twitter['followers_count'] ) ){
			set_transient( 'twitter_count' , $twitter['followers_count'] , 1200);
			if( get_option( 'followers_count') != $twitter['followers_count'] ) 
				update_option( 'followers_count' , $twitter['followers_count'] );
		}
			
		if( $twitter['followers_count'] == 0 && get_option( 'followers_count') )
			$twitter['followers_count'] = get_option( 'followers_count');
				
		elseif( $twitter['followers_count'] == 0 && !get_option( 'followers_count') )
			$twitter['followers_count'] = 0;
	}
	return $twitter;
}

function tie_facebook_fans( $page_link ){
	$face_link = @parse_url($page_link);

	if( $face_link['host'] == 'www.facebook.com' || $face_link['host']  == 'facebook.com' ){
		$fans = get_transient('fans_count');
		if( empty( $fans ) ){
			try {
				$data = @json_decode(tie_remote_get("http://graph.facebook.com/".$page_link));
				$fans = $data->likes;
			} catch (Exception $e) {
				$fans = 0;
			}
				
			if( !empty($fans) ){
				set_transient( 'fans_count' , $fans , 1200);
				if ( get_option( 'fans_count') != $fans )
					update_option( 'fans_count' , $fans );
			}
				
			if( $fans == 0 && get_option( 'fans_count') )
				$fans = get_option( 'fans_count');
					
			elseif( $fans == 0 && !get_option( 'fans_count') )
				$fans = 0;
		}	
		return $fans;
	}
}

function tie_youtube_subs( $channel_link ){
	$youtube_link = @parse_url($channel_link);

	if( $youtube_link['host'] == 'www.youtube.com' || $youtube_link['host']  == 'youtube.com' ){
		$subs = get_transient('youtube_count');
		if( empty( $subs ) ){
			try {
				if (strpos( strtolower($channel_link) , "channel") === false)
					$youtube_name = substr(@parse_url($channel_link, PHP_URL_PATH), 6);
				else
					$youtube_name = substr(@parse_url($channel_link, PHP_URL_PATH), 9);

				$json = @tie_remote_get("http://gdata.youtube.com/feeds/api/users/".$youtube_name."?alt=json");
				$data = json_decode($json, true); 
				$subs = $data['entry']['yt$statistics']['subscriberCount']; 
			} catch (Exception $e) {
				$subs = 0;
			}
			
			if( !empty($subs) ){
				set_transient( 'youtube_count' , $subs , 1200);
				if( get_option( 'youtube_count') != $subs )
					update_option( 'youtube_count' , $subs );
			}
				
			if( $subs == 0 && get_option( 'youtube_count') )
				$subs = get_option( 'youtube_count');
					
			elseif( $subs == 0 && !get_option( 'youtube_count') )
				$subs = 0;
		}
		return $subs;
	}
}

function tie_vimeo_count( $page_link ) {
	$vimeo_link = @parse_url($page_link);

	if( $vimeo_link['host'] == 'www.vimeo.com' || $vimeo_link['host']  == 'vimeo.com' ){
		$vimeo = get_transient('vimeo_count');
		if( empty( $vimeo ) ){
			try {
				$page_name = substr(@parse_url($page_link, PHP_URL_PATH), 10);
				@$data = @json_decode(tie_remote_get( 'http://vimeo.com/api/v2/channel/' . $page_name  .'/info.json'));
			
				$vimeo = $data->total_subscribers;
			} catch (Exception $e) {
				$vimeo = 0;
			}

			if( !empty($vimeo) ){
				set_transient( 'vimeo_count' , $vimeo , 1200);
				if( get_option( 'vimeo_count') != $vimeo )
					update_option( 'vimeo_count' , $vimeo );
			}
				
			if( $vimeo == 0 && get_option( 'vimeo_count') )
				$vimeo = get_option( 'vimeo_count');
			elseif( $vimeo == 0 && !get_option( 'vimeo_count') )
				$vimeo = 0;
		}
		return $vimeo;
	}
}

function tie_dribbble_count( $page_link ) {
	$dribbble_link = @parse_url($page_link);

	if( $dribbble_link['host'] == 'www.dribbble.com' || $dribbble_link['host']  == 'dribbble.com' ){
		$dribbble = get_transient('dribbble_count');
		if( empty( $dribbble ) ){
			try {
				$page_name = substr(@parse_url($page_link, PHP_URL_PATH), 1);
				@$data = @json_decode(tie_remote_get( 'http://api.dribbble.com/' . $page_name));
			
				$dribbble = $data->followers_count;
			} catch (Exception $e) {
				$dribbble = 0;
			}

			if( !empty($dribbble) ){
				set_transient( 'dribbble_count' , $dribbble , 1200);
				if( get_option( 'dribbble_count') != $dribbble )
					update_option( 'dribbble_count' , $dribbble );
			}
				
			if( $dribbble == 0 && get_option( 'dribbble_count') )
				$dribbble = get_option( 'dribbble_count');
			elseif( $dribbble == 0 && !get_option( 'dribbble_count') )
				$dribbble = 0;
		}
		return $dribbble;
	}
}

function tie_soundcloud_count( $page_link , $api ) {
	$soundcloud_link = @parse_url($page_link);
	if( $soundcloud_link['host'] == 'www.soundcloud.com' || $soundcloud_link['host']  == 'soundcloud.com' ){
		$soundcloud = get_transient('soundcloud_count');
		if( empty( $soundcloud ) ){
			try {
				$username = substr( $soundcloud_link['path'] , 1);
				$data = @json_decode(tie_remote_get("http://api.soundcloud.com/users/$username.json?consumer_key=$api") , true );
				$soundcloud = (int) $data['followers_count'];
			
			} catch (Exception $e) {
				$soundcloud = 0;
			}

			if( !empty($soundcloud) ){
				set_transient( 'soundcloud_count' , $soundcloud , 1200);
				if( get_option( 'soundcloud_count') != $soundcloud )
					update_option( 'soundcloud_count' , $soundcloud );
			}
			
			if( $soundcloud == 0 && get_option( 'soundcloud_count') )
				$soundcloud = get_option( 'soundcloud_count');
			elseif( $soundcloud == 0 && !get_option( 'soundcloud_count') )
				$soundcloud = 0;
		}
		return $soundcloud;
	}	
}

function tie_behance_count( $page_link , $api ) {
	$behance_link = @parse_url($page_link);
	if( $behance_link['host'] == 'www.behance.net' || $behance_link['host']  == 'behance.net' ){
		$behance = get_transient('behance_count');
		if( empty( $behance ) ){
			try {
				$username = substr( $behance_link['path'] , 1);
				$data = @json_decode( tie_remote_get("http://www.behance.net/v2/users/$username?api_key=$api") , true );
				$behance = (int) $data['user']['stats']['followers'];		
			} catch (Exception $e) {
				$behance = 0;
			}

			if( !empty($behance) ){
				set_transient( 'behance_count' , $behance , 1200);
				if( get_option( 'behance_count') != $behance )
					update_option( 'behance_count' , $behance );
			}
			
			if( $behance == 0 && get_option( 'behance_count') )
				$behance = get_option( 'behance_count');
			elseif( $behance == 0 && !get_option( 'behance_count') )
				$behance = 0;
		}
		return $behance;
	}	
}

function tie_instagram_count( $page_link , $api ) {
	$instagram_link = @parse_url($page_link);
	if( $instagram_link['host'] == 'www.instagram.com' || $instagram_link['host']  == 'instagram.com' ){
		$instagram = get_transient('instagram_count');
		if( empty( $instagram ) ){
			try {
				$username = explode(".", $api);
				$data = @json_decode( tie_remote_get("https://api.instagram.com/v1/users/$username[0]/?access_token=$api") , true );
				$instagram = (int) $data['data']['counts']['followed_by'];	
			
			} catch (Exception $e) {
				$instagram = 0;
			}

			if( !empty($instagram) ){
				set_transient( 'instagram_count' , $instagram , 1200);
				if( get_option( 'instagram_count') != $instagram )
					update_option( 'instagram_count' , $instagram );
			}
			
			if( $instagram == 0 && get_option( 'instagram_count') )
				$instagram = get_option( 'instagram_count');
			elseif( $instagram == 0 && !get_option( 'instagram_count') )
				$instagram = 0;
		}
		return $instagram;
	}	
}


/*-----------------------------------------------------------------------------------*/
# Social 
/*-----------------------------------------------------------------------------------*/
function tie_get_social($newtab='yes', $icon_size='32', $tooltip='ttip'){
	$social = tie_get_option('social');
	@extract($social);
		
	if ($newtab == 'yes') $newtab = "target=\"_blank\"";
	else $newtab = '';
		
	$icons_path =  get_template_directory_uri().'/images/socialicons';	?>
		<div class="social-icons icon_<?php echo $icon_size; ?>">
		<?php
		// RSS
		if ( !tie_get_option('rss_icon') ){
		if ( tie_get_option('rss_url') != '' && tie_get_option('rss_url') != ' ' ) $rss = tie_get_option('rss_url') ;
		else $rss = get_bloginfo('rss2_url'); 
			?><a class="<?php echo $tooltip; ?>" title="Rss" href="<?php echo $rss ; ?>" <?php echo $newtab; ?>><i class="tieicon-rss"></i></a><?php 
		}
		// Google+
		if ( !empty($google_plus) && $google_plus != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Google+" href="<?php echo $google_plus; ?>" <?php echo $newtab; ?>><i class="tieicon-gplus"></i></a><?php 
		}
		// Facebook
		if ( !empty($facebook) && $facebook != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Facebook" href="<?php echo $facebook; ?>" <?php echo $newtab; ?>><i class="tieicon-facebook"></i></a><?php 
		}
		// Twitter
		if ( !empty($twitter) && $twitter != ' ') {
			?><a class="<?php echo $tooltip; ?>" title="Twitter" href="<?php echo $twitter; ?>" <?php echo $newtab; ?>><i class="tieicon-twitter"></i></a><?php
		}		
		// Pinterest
		if ( !empty($Pinterest) && $Pinterest != ' ') {
			?><a class="<?php echo $tooltip; ?>" title="Pinterest" href="<?php echo $Pinterest; ?>" <?php echo $newtab; ?>><i class="tieicon-pinterest-circled"></i></a><?php
		}
		// MySpace
		if ( !empty($myspace) && $myspace != ' ') {
			?><a class="<?php echo $tooltip; ?>" title="MySpace" href="<?php echo $myspace; ?>" <?php echo $newtab; ?>><i class="tieicon-myspace"></i></a><?php
		}
		// FriendFeed
		if ( !empty($friendfeed) && $friendfeed != ' ') {
			?><a class="<?php echo $tooltip; ?>" title="FriendFeed" href="<?php echo $friendfeed; ?>" <?php echo $newtab; ?>><i class="tieicon-friendfeed"></i></a><?php
		}
		// dribbble
		if ( !empty($dribbble) && $dribbble != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Dribbble" href="<?php echo $dribbble; ?>" <?php echo $newtab; ?>><i class="tieicon-dribbble"></i></a><?php
		}
		// LinkedIN
		if ( !empty($linkedin) && $linkedin != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="LinkedIn" href="<?php echo $linkedin; ?>" <?php echo $newtab; ?>><i class="tieicon-linkedin"></i></a><?php
		}
		// evernote
		if ( !empty($evernote) && $evernote != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Evernote" href="<?php echo $evernote; ?>" <?php echo $newtab; ?>><i class="tieicon-evernote"></i></a><?php
		}
		// Flickr
		if ( !empty($flickr) && $flickr != ' ') {
			?><a class="<?php echo $tooltip; ?>" title="Flickr" href="<?php echo $flickr; ?>" <?php echo $newtab; ?>><i class="tieicon-flickr"></i></a><?php
		}
		// Picasa
		if ( !empty($picasa) && $picasa != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Picasa" href="<?php echo $picasa; ?>" <?php echo $newtab; ?>><i class="tieicon-picasa"></i></a><?php
		}
		// YouTube
		if ( !empty($youtube) && $youtube != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Youtube" href="<?php echo $youtube; ?>" <?php echo $newtab; ?>><i class="tieicon-youtube"></i></a><?php
		}
		// Skype
		if ( !empty($skype) && $skype != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Skype" href="<?php echo $skype; ?>" <?php echo $newtab; ?>><i class="tieicon-skype"></i></a><?php
		}
		// Digg
		if ( !empty($digg) && $digg != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Digg" href="<?php echo $digg; ?>" <?php echo $newtab; ?>><i class="tieicon-digg"></i></a><?php
		}
		// Reddit 
		if ( !empty($reddit) && $reddit != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Reddit" href="<?php echo $reddit; ?>" <?php echo $newtab; ?>><i class="tieicon-reddit"></i></a><?php
		}
		// Delicious 
		if ( !empty($delicious) && $delicious != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Delicious" href="<?php echo $delicious; ?>" <?php echo $newtab; ?>><i class="tieicon-delicious"></i></a><?php
		}
		// stumbleuponUpon 
		if ( !empty($stumbleupon) && $stumbleupon != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="StumbleUpon" href="<?php echo $stumbleupon; ?>" <?php echo $newtab; ?>><i class="tieicon-stumbleupon"></i></a><?php
		}
		// Tumblr 
		if ( !empty($tumblr) && $tumblr != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Tumblr" href="<?php echo $tumblr; ?>" <?php echo $newtab; ?>><i class="tieicon-tumblr"></i></a><?php
		}
		// Vimeo
		if ( !empty($vimeo) && $vimeo != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Vimeo" href="<?php echo $vimeo; ?>" <?php echo $newtab; ?>><i class="tieicon-vimeo"></i></a><?php
		}
		// Blogger
		if ( !empty($blogger) && $blogger != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Blogger" href="<?php echo $blogger; ?>" <?php echo $newtab; ?>><i class="tieicon-blogger"></i></a><?php
		}
		// Wordpress
		if ( !empty($wordpress) && $wordpress != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="WordPress" href="<?php echo $wordpress; ?>" <?php echo $newtab; ?>><i class="tieicon-wordpress"></i></a><?php
		}
		// Yelp
		if ( !empty($yelp) && $yelp != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Yelp" href="<?php echo $yelp; ?>" <?php echo $newtab; ?>><i class="tieicon-yelp"></i></a><?php
		}
		// Last.fm
		if ( !empty($lastfm) && $lastfm != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Last.fm" href="<?php echo $lastfm; ?>" <?php echo $newtab; ?>><i class="tieicon-lastfm"></i></a><?php
		}
		// grooveshark
		if ( !empty($grooveshark) && $grooveshark != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Grooveshark" href="<?php echo $grooveshark; ?>" <?php echo $newtab; ?>><i class="tieicon-grooveshark"></i></a><?php
		}
		// sharethis
		if ( !empty($sharethis) && $sharethis != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="ShareThis" href="<?php echo $sharethis; ?>" <?php echo $newtab; ?>><i class="tieicon-share"></i></a><?php
		}
		// dropbox
		if ( !empty($dropbox) && $dropbox != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Dropbox" href="<?php echo $dropbox; ?>" <?php echo $newtab; ?>><i class="tieicon-dropbox"></i></a><?php
		}
		// xing.me
		if ( !empty($xing) && $xing != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Xing" href="<?php echo $xing; ?>" <?php echo $newtab; ?>><i class="tieicon-xing"></i></a><?php
		}
		// DeviantArt
		if ( !empty($deviantart) && $deviantart != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="DeviantArt" href="<?php echo $deviantart; ?>" <?php echo $newtab; ?>><i class="tieicon-deviantart"></i></a><?php
		}
		// Apple
		if ( !empty($apple) && $apple != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Apple" href="<?php echo $apple; ?>" <?php echo $newtab; ?>><i class="tieicon-apple"></i></a><?php
		}
		// foursquare
		if ( !empty($foursquare) && $foursquare != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Foursquare" href="<?php echo $foursquare; ?>" <?php echo $newtab; ?>><i class="tieicon-foursquare"></i></a><?php
		}
		// github
		if ( !empty($github) && $github != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Github" href="<?php echo $github; ?>" <?php echo $newtab; ?>><i class="tieicon-github"></i></a><?php
		}
		// soundcloud
		if ( !empty($soundcloud) && $soundcloud != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="SoundCloud" href="<?php echo $soundcloud; ?>" <?php echo $newtab; ?>><i class="tieicon-soundcloud"></i></a><?php
		}		
		// behance
		if ( !empty( $behance ) && $behance != '' && $behance != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Behance" href="<?php echo $behance; ?>" <?php echo $newtab; ?>><i class="tieicon-behance"></i></a><?php
		}
		// instagram
		if ( !empty( $instagram ) && $instagram != '' && $instagram != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="instagram" href="<?php echo $instagram; ?>" <?php echo $newtab; ?>><i class="tieicon-instagram"></i></a><?php
		}
		// paypal
		if ( !empty( $paypal ) && $paypal != '' && $paypal != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="paypal" href="<?php echo $paypal; ?>" <?php echo $newtab; ?>><i class="tieicon-paypal"></i></a><?php
		}
		// spotify
		if ( !empty( $spotify ) && $spotify != '' && $spotify != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="spotify" href="<?php echo $spotify; ?>" <?php echo $newtab; ?>><i class="tieicon-spotify"></i></a><?php
		}
		// viadeo
		if ( !empty( $viadeo ) && $viadeo != '' && $viadeo != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="viadeo" href="<?php echo $viadeo; ?>" <?php echo $newtab; ?>><i class="tieicon-viadeo"></i></a><?php
		}
		// Google Play
		if ( !empty( $google_play ) && $google_play != '' && $google_play != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Google Play" href="<?php echo $google_play; ?>" <?php echo $newtab; ?>><i class="tieicon-googleplay"></i></a><?php
		}
		// 500PX
		if ( !empty($px500) && $px500 != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="500px" href="<?php echo $px500; ?>" <?php echo $newtab; ?>><i class="tieicon-fivehundredpx"></i></a><?php
		}
		// Forrst
		if ( !empty($forrst) && $forrst != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="Forrst" href="<?php echo $forrst; ?>" <?php echo $newtab; ?>><i class="tieicon-forrst"></i></a><?php
		}
		// VK
		if ( !empty($vk) && $vk != ' ' ) {
			?><a class="<?php echo $tooltip; ?>" title="vk.com" href="<?php echo $vk; ?>" <?php echo $newtab; ?>><i class="tieicon-vkontakte"></i></a><?php
		} ?>
	</div>

<?php
}

/*-----------------------------------------------------------------------------------*/
# Read More Functions
/*-----------------------------------------------------------------------------------*/
function tie_remove_excerpt( $more ) {
	return ' ...';
}
add_filter('excerpt_more', 'tie_remove_excerpt');


/*-----------------------------------------------------------------------------------*/
# Page Navigation
/*-----------------------------------------------------------------------------------*/
function tie_pagenavi( $type = 'num' ){ ?>
		<div class="pagination">
			<?php tie_get_pagenavi(); ?>
		</div>
	<?php	
}


/*-----------------------------------------------------------------------------------*/
# Tie Excerpt
/*-----------------------------------------------------------------------------------*/
function tie_excerpt($text, $chars = 120) {
	$text = $text." ";
	$text = mb_substr( $text , 0 , $chars , 'UTF-8');
	$text = $text."...";
	return $text;
}


/*-----------------------------------------------------------------------------------*/
# Queue Comments reply js
/*-----------------------------------------------------------------------------------*/
function tie_comments_queue_js(){
if ( (!is_admin()) && is_singular() && comments_open() && get_option('thread_comments') )
  wp_enqueue_script( 'comment-reply' );
}
add_action('wp_print_scripts', 'tie_comments_queue_js');


/*-----------------------------------------------------------------------------------*/
# Remove recent comments_ style
/*-----------------------------------------------------------------------------------*/
function tie_remove_recent_comments_style() {
	add_filter( 'show_recent_comments_widget_style', '__return_false' );
}
add_action( 'widgets_init', 'tie_remove_recent_comments_style' );


/*-----------------------------------------------------------------------------------*/
# Get the thumbnail
/*-----------------------------------------------------------------------------------*/
function tie_full_image(){
	global $post ;
	if ( has_post_thumbnail($post->ID) ){
		$image_id = get_post_thumbnail_id($post->ID);  
		$image_url = wp_get_attachment_image_src($image_id,'full');  
		return $image_url = $image_url[0];
	}
}


/*-----------------------------------------------------------------------------------*/
# tie Thumb SRC
/*----------------------------------------------------------------------------------*/
function tie_thumb_src( $size ){
	global $post;
	$image_id = get_post_thumbnail_id($post->ID);  
	$image_url = wp_get_attachment_image_src( $image_id, $size );  
	return $image_url[0];
	
}


/*-----------------------------------------------------------------------------------*/
# Add user's social accounts
/*-----------------------------------------------------------------------------------*/
add_action( 'show_user_profile', 'tie_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'tie_show_extra_profile_fields' );
function tie_show_extra_profile_fields( $user ) { ?>
	<h3><?php _e( 'Social Networking', 'tie' ) ?></h3>
	<table class="form-table">
		<tr>
			<th><label for="google">Google + URL</label></th>
			<td>
				<input type="text" name="google" id="google" value="<?php echo esc_attr( get_the_author_meta( 'google', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="twitter">Twitter Username</label></th>
			<td>
				<input type="text" name="twitter" id="twitter" value="<?php echo esc_attr( get_the_author_meta( 'twitter', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="facebook">FaceBook URL</label></th>
			<td>
				<input type="text" name="facebook" id="facebook" value="<?php echo esc_attr( get_the_author_meta( 'facebook', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="linkedin">linkedIn URL</label></th>
			<td>
				<input type="text" name="linkedin" id="linkedin" value="<?php echo esc_attr( get_the_author_meta( 'linkedin', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="flickr">Flickr URL</label></th>
			<td>
				<input type="text" name="flickr" id="flickr" value="<?php echo esc_attr( get_the_author_meta( 'flickr', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="youtube">YouTube URL</label></th>
			<td>
				<input type="text" name="youtube" id="youtube" value="<?php echo esc_attr( get_the_author_meta( 'youtube', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>
		<tr>
			<th><label for="pinterest">Pinterest URL</label></th>
			<td>
				<input type="text" name="pinterest" id="pinterest" value="<?php echo esc_attr( get_the_author_meta( 'pinterest', $user->ID ) ); ?>" class="regular-text" /><br />
			</td>
		</tr>

	</table>
<?php }

## Save user's social accounts
add_action( 'personal_options_update', 'tie_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'tie_save_extra_profile_fields' );
function tie_save_extra_profile_fields( $user_id ) {
	if ( !current_user_can( 'edit_user', $user_id ) ) return false;
	update_user_meta( $user_id, 'google', $_POST['google'] );
	update_user_meta( $user_id, 'pinterest', $_POST['pinterest'] );
	update_user_meta( $user_id, 'twitter', $_POST['twitter'] );
	update_user_meta( $user_id, 'facebook', $_POST['facebook'] );
	update_user_meta( $user_id, 'linkedin', $_POST['linkedin'] );
	update_user_meta( $user_id, 'flickr', $_POST['flickr'] );
	update_user_meta( $user_id, 'youtube', $_POST['youtube'] );
}


/*-----------------------------------------------------------------------------------*/
# Get Feeds 
/*-----------------------------------------------------------------------------------*/
function tie_get_feeds( $feed , $number = 10 ){
	include_once(ABSPATH . WPINC . '/feed.php');
	$rss = @fetch_feed( $feed );
	if (!is_wp_error( $rss ) ){
		$maxitems = $rss->get_item_quantity($number); 
		$rss_items = $rss->get_items(0, $maxitems); 
	}
	if ($maxitems == 0) {
		$out = "<ul><li>". __( 'No items.', 'tie' )."</li></ul>";
	}else{
		$out = "<ul>";
		
		foreach ( $rss_items as $item ) : 
			$out .= '<li><a href="'. esc_url( $item->get_permalink() ) .'" title="'.  __( "Posted ", "tie" ).$item->get_date("j F Y | g:i a").'">'. esc_html( $item->get_title() ) .'</a></li>';
		endforeach;
		$out .='</ul>';
	}
	return $out;
}


/*-----------------------------------------------------------------------------------*/
# Tie Wp Footer
/*-----------------------------------------------------------------------------------*/
add_action('wp_footer', 'tie_wp_footer');
function tie_wp_footer() { 
	if ( tie_get_option('footer_code')) echo htmlspecialchars_decode( stripslashes(tie_get_option('footer_code') )); ?>
	<?php
} 


/*-----------------------------------------------------------------------------------*/
# Get Most Racent posts
/*-----------------------------------------------------------------------------------*/
function tie_last_posts($numberOfPosts = 5 , $thumb = true){
	global $post;
	$orig_post = $post;
	
	$lastPosts = get_posts('no_found_rows=1&numberposts='.$numberOfPosts);
	foreach($lastPosts as $post): setup_postdata($post);
?>
<li>
	<?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() && $thumb ) : ?>			
		<div class="post-thumbnail">
			<a href="<?php the_permalink(); ?>" title="<?php printf( __( 'Permalink to %s', 'tie' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_post_thumbnail( 'tie-small' ); ?></a>
		</div><!-- post-thumbnail /-->
	<?php else:
		$format = get_post_format();
		if( false === $format ) { $format = 'standard'; }
	?>
		<div class="post-thumbnail no-thumbnail format-<?php echo $format ?>">
			<a href="<?php the_permalink(); ?>" title="<?php printf( __( 'Permalink to %s', 'tie' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"></a>
		</div><!-- post-thumbnail /-->	
	<?php endif; ?>
	<h3><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h3>
	<span class="date"><?php tie_get_time(); ?></span>
</li>
<?php endforeach; 
	$post = $orig_post;
}


/*-----------------------------------------------------------------------------------*/
# Get Most Racent posts from Category
/*-----------------------------------------------------------------------------------*/
function tie_last_posts_cat($numberOfPosts = 5 , $thumb = true , $cats = 1){
	global $post;
	$orig_post = $post;

	$lastPosts = get_posts('category='.$cats.'&no_found_rows=1&numberposts='.$numberOfPosts);
	foreach($lastPosts as $post): setup_postdata($post);
?>
<li>
	<?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() && $thumb ) : ?>			
		<div class="post-thumbnail">
			<a href="<?php the_permalink(); ?>" title="<?php printf( __( 'Permalink to %s', 'tie' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_post_thumbnail( 'tie-small' ); ?></a>
		</div><!-- post-thumbnail /-->
	<?php else:
		$format = get_post_format();
		if( false === $format ) { $format = 'standard'; }
	?>
		<div class="post-thumbnail no-thumbnail format-<?php echo $format ?>">
			<a href="<?php the_permalink(); ?>" title="<?php printf( __( 'Permalink to %s', 'tie' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"></a>
		</div><!-- post-thumbnail /-->
	<?php endif; ?>
	<h3><a href="<?php the_permalink(); ?>"><?php the_title();?></a></h3>
	<span class="date"><?php tie_get_time();  ?></span>
</li>
<?php endforeach;
	$post = $orig_post;
}


/*-----------------------------------------------------------------------------------*/
# Get Random posts 
/*-----------------------------------------------------------------------------------*/
function tie_random_posts($numberOfPosts = 5 , $thumb = true){
	global $post;
	$orig_post = $post;

	$lastPosts = get_posts('orderby=rand&no_found_rows=1&numberposts='.$numberOfPosts);
	foreach($lastPosts as $post): setup_postdata($post);
?>
<li>
	<?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() && $thumb ) : ?>			
		<div class="post-thumbnail">
			<a href="<?php the_permalink(); ?>" title="<?php printf( __( 'Permalink to %s', 'tie' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_post_thumbnail( 'tie-small' ); ?></a>
		</div><!-- post-thumbnail /-->
	<?php else:
		$format = get_post_format();
		if( false === $format ) { $format = 'standard'; }
	?>
		<div class="post-thumbnail no-thumbnail format-<?php echo $format ?>">
			<a href="<?php the_permalink(); ?>" title="<?php printf( __( 'Permalink to %s', 'tie' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"></a>
		</div><!-- post-thumbnail /-->
	<?php endif; ?>
	<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
	<span class="date"><?php tie_get_time(); ?></span>
</li>
<?php endforeach;
	$post = $orig_post;
}


/*-----------------------------------------------------------------------------------*/
# Get Popular posts 
/*-----------------------------------------------------------------------------------*/
function tie_popular_posts($pop_posts = 5 , $thumb = true){
	global $wpdb , $post;
	$orig_post = $post;
	
	$popularposts = "SELECT ID,post_title,post_date,post_author,post_content,post_type FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' ORDER BY comment_count DESC LIMIT 0,".$pop_posts;
	$posts = $wpdb->get_results($popularposts);
	if($posts){
		global $post;
		foreach($posts as $post){
		setup_postdata($post);?>
			<li>
			<?php if ( function_exists("has_post_thumbnail") && has_post_thumbnail() && $thumb ) : ?>			
				<div class="post-thumbnail">
					<a href="<?php echo get_permalink( $post->ID ) ?>" title="<?php printf( __( 'Permalink to %s', 'tie' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_post_thumbnail( 'tie-small' ); ?></a>
				</div><!-- post-thumbnail /-->
			<?php else:
				$format = get_post_format();
				if( false === $format ) { $format = 'standard'; }
			?>
				<div class="post-thumbnail no-thumbnail format-<?php echo $format ?>">
					<a href="<?php the_permalink(); ?>" title="<?php printf( __( 'Permalink to %s', 'tie' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"></a>
		</div><!-- post-thumbnail /-->
			<?php endif; ?>
				<h3><a href="<?php echo get_permalink( $post->ID ) ?>" title="<?php echo the_title(); ?>"><?php echo the_title(); ?></a></h3>
				<span class="date"><?php tie_get_time();  ?></span>
			</li>
	<?php 
		}
	}
	$post = $orig_post;
}


/*-----------------------------------------------------------------------------------*/
# Get Most commented posts 
/*-----------------------------------------------------------------------------------*/
function tie_most_commented($comment_posts = 5 , $avatar_size = 50){
$comments = get_comments('status=approve&number='.$comment_posts);
foreach ($comments as $comment) { ?>
	<li>
		<div class="post-thumbnail">
			<?php echo get_avatar( $comment, $avatar_size ); ?>
		</div>
		<a href="<?php echo get_permalink($comment->comment_post_ID ); ?>#comment-<?php echo $comment->comment_ID; ?>">
		<?php echo strip_tags($comment->comment_author); ?>: <?php echo wp_html_excerpt( $comment->comment_content, 60 ); ?>... </a>
	</li>
<?php } 
}


/*-----------------------------------------------------------------------------------*/
# Soundcloud Function
/*-----------------------------------------------------------------------------------*/
function tie_soundcloud($url , $autoplay = 'false' ) {
	global $post;
	$get_meta = get_post_custom($post->ID);
	$tie_post_color = $get_meta["tie_post_color"][0];
	$color = '';
	if( empty($tie_post_color) ){
		if( is_category() ){
			$cat_id = get_query_var('cat');
		}
		else{
			$category = get_the_category($post->ID);
			$cat_id = $category[0]->cat_ID;
		}
		$cat_option = get_option('tie_cat_'.$cat_id);
		$tie_post_color = $cat_option['tie_post_color'];
	}
	if( empty($tie_post_color) && tie_get_option( 'theme_color' ) ) $tie_post_color = tie_get_option( 'theme_color' );
	
	if( !empty( $tie_post_color ) ){
		$tie_post_color = str_replace ( '#' , '' , tie_get_option( $tie_post_color ) );
		$color = '&amp;color='.$tie_post_color;
	}
	return '<iframe style="width:100%" height="166" src="https://w.soundcloud.com/player/?url='.$url.$color.'&amp;auto_play='.$autoplay.'&amp;show_artwork=true"></iframe>';
}

/*-----------------------------------------------------------------------------------*/
# Google Map Function
/*-----------------------------------------------------------------------------------*/
function tie_google_maps($src , $width = 610 , $height = 440) {
	return '<div class="google-map"><iframe width="'.$width.'" height="'.$height.'" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="'.$src.'&amp;output=embed"></iframe></div>';
}

/*-----------------------------------------------------------------------------------*/
# Login Form
/*-----------------------------------------------------------------------------------*/
function tie_login_form( $login_only  = 0 ) {
	global $user_ID, $user_identity, $user_level;
	
	if ( $user_ID ) : ?>
		<?php if( empty( $login_only ) ): ?>
		<div id="user-login">
			<p class="welcome-text"><?php _e( 'Welcome' , 'tie' ) ?> <strong><?php echo $user_identity ?></strong> .</p>
			<span class="author-avatar"><?php echo get_avatar( $user_ID, $size = '85'); ?></span>
			<ul>
				<li><a href="<?php echo home_url() ?>/wp-admin/"><?php _e( 'Dashboard' , 'tie' ) ?> </a></li>
				<li><a href="<?php echo home_url() ?>/wp-admin/profile.php"><?php _e( 'Your Profile' , 'tie' ) ?> </a></li>
				<li><a href="<?php echo wp_logout_url(); ?>"><?php _e( 'Logout' , 'tie' ) ?> </a></li>
			</ul>
			<div class="social-icons-widget author-social">
				<?php if ( get_the_author_meta( 'twitter' , $user_ID) ) : ?>
				<a class="ttip" href="http://twitter.com/<?php the_author_meta( 'twitter' ); ?>" title="<?php echo $user_identity ?> <?php _e( '  on Twitter', 'tie' ); ?>"><i class="tieicon-twitter"></i></a>
				<?php endif ?>	
				<?php if ( get_the_author_meta( 'facebook' , $user_ID) ) : ?>
				<a class="ttip" href="<?php the_author_meta( 'facebook' ); ?>" title="<?php echo $user_identity ?> <?php _e( '  on Facebook', 'tie' ); ?>"><i class="tieicon-facebook"></i></a>
				<?php endif ?>
				<?php if ( get_the_author_meta( 'google' , $user_ID) ) : ?>
				<a class="ttip" href="<?php the_author_meta( 'google' ); ?>" title="<?php echo $user_identity ?> <?php _e( '  on Google+', 'tie' ); ?>"><i class="tieicon-gplus"></i></a>
				<?php endif ?>	
				<?php if ( get_the_author_meta( 'linkedin' , $user_ID) ) : ?>
				<a class="ttip" href="<?php the_author_meta( 'linkedin' , $user_ID); ?>" title="<?php echo $user_identity ?> <?php _e( '  on Linkedin', 'tie' ); ?>"><i class="tieicon-linkedin"></i></a>
				<?php endif ?>				
				<?php if ( get_the_author_meta( 'flickr' , $user_ID) ) : ?>
				<a class="ttip" href="<?php the_author_meta( 'flickr' , $user_ID); ?>" title="<?php echo $user_identity ?> <?php _e( '  on Flickr', 'tie' ); ?>"><i class="tieicon-flickr"></i></a>
				<?php endif ?>	
				<?php if ( get_the_author_meta( 'youtube' , $user_ID) ) : ?>
				<a class="ttip" href="<?php the_author_meta( 'youtube' , $user_ID); ?>" title="<?php echo $user_identity ?> <?php _e( '  on YouTube', 'tie' ); ?>"><i class="tieicon-youtube"></i></a>
				<?php endif ?>
				<?php if ( get_the_author_meta( 'pinterest' , $user_ID) ) : ?>
				<a class="ttip" href="<?php the_author_meta( 'pinterest' , $user_ID); ?>" title="<?php echo $user_identity ?> <?php _e( '  on Pinterest', 'tie' ); ?>"><i class="tieicon-pinterest-circled"></i></a>
				<?php endif ?>	
			</div>
			<div class="clear"></div>
		</div>
		<?php endif; ?>
	<?php else: ?>
		<div id="login-form">
			<form action="<?php echo home_url() ?>/wp-login.php" method="post">
				<p id="log-username"><input type="text" name="log" id="log" value="<?php _e( 'Username' , 'tie' ) ?>" onfocus="if (this.value == '<?php _e( 'Username' , 'tie' ) ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php _e( 'Username' , 'tie' ) ?>';}"  size="33" /></p>
				<p id="log-pass"><input type="password" name="pwd" id="pwd" value="<?php _e( 'Password' , 'tie' ) ?>" onfocus="if (this.value == '<?php _e( 'Password' , 'tie' ) ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php _e( 'Password' , 'tie' ) ?>';}" size="33" /></p>
				<input type="submit" name="submit" value="<?php _e( 'Log in' , 'tie' ) ?>" class="login-button" />
				<label for="rememberme"><input name="rememberme" id="rememberme" type="checkbox" checked="checked" value="forever" /> <?php _e( 'Remember Me' , 'tie' ) ?></label>
				<input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>"/>
			</form>
			<ul class="login-links">
				<?php if ( get_option('users_can_register') ) : ?><?php echo wp_register() ?><?php endif; ?>
				<li><a href="<?php echo wp_lostpassword_url( get_permalink() ); ?>"><?php _e( 'Lost your password?' , 'tie' ) ?></a></li>
			</ul>
		</div>
	<?php endif;
}


/*-----------------------------------------------------------------------------------*/
# Get Og Image of post
/*-----------------------------------------------------------------------------------*/
function tie_og_image() {
	global $post ;
	
	if ( function_exists("has_post_thumbnail") && has_post_thumbnail() )
		$post_thumb = tie_thumb_src( 'tie-post' ) ;
	elseif( get_post_format() == 'video' ){
		$get_meta = get_post_custom($post->ID);
		$video_url = $get_meta["tie_video_url"][0];
		$video_link = @parse_url($video_url);
		if ( $video_link['host'] == 'www.youtube.com' || $video_link['host']  == 'youtube.com' ) {
			parse_str( @parse_url( $video_url, PHP_URL_QUERY ), $my_array_of_vars );
			$video =  $my_array_of_vars['v'] ;
			$post_thumb ='http://img.youtube.com/vi/'.$video.'/0.jpg';
		}
		elseif( $video_link['host'] == 'www.vimeo.com' || $video_link['host']  == 'vimeo.com' ){
			$video = (int) substr(@parse_url($video_url, PHP_URL_PATH), 1);
			$url = 'http://vimeo.com/api/v2/video/'.$video.'.php';;
			$contents = @file_get_contents($url);
			$thumb = @unserialize(trim($contents));
			$post_thumb = $thumb[0][thumbnail_large];
		}
	}
	
	if( isset($post_thumb) )
		echo '<meta property="og:image" content="'. $post_thumb .'" />';
}

	
/*-----------------------------------------------------------------------------------*/
# Get the post time
/*-----------------------------------------------------------------------------------*/
function tie_get_time(){
	global $post ;
	
	$to = time();
	$from = get_the_time('U') ;
	
	$diff = (int) abs($to - $from);
	if ($diff <= 3600) {
		$mins = round($diff / 60);
		if ($mins <= 1) {
			$mins = 1;
		}
		$since = sprintf(_n('%s min', '%s mins', $mins), $mins) .' '. __( 'ago' , 'tie' );
	}
	else if (($diff <= 86400) && ($diff > 3600)) {
		$hours = round($diff / 3600);
		if ($hours <= 1) {
			$hours = 1;
		}
		$since = sprintf(_n('%s hour', '%s hours', $hours), $hours) .' '. __( 'ago' , 'tie' );
	}
	elseif ($diff >= 86400) {
		$days = round($diff / 86400);
		if ($days <= 1) {
			$days = 1;
			$since = sprintf(_n('%s day', '%s days', $days), $days) .' '. __( 'ago' , 'tie' );
		}
		elseif( $days > 29){
			$since = get_the_time(get_option('date_format'));
		}
		else{
			$since = sprintf(_n('%s day', '%s days', $days), $days) .' '. __( 'ago' , 'tie' );
		}
	}
	echo $since;
}



/*-----------------------------------------------------------------------------------*/
# Add Class to Gallery shortcode for lightbox
/*-----------------------------------------------------------------------------------*/
add_filter( 'post_gallery', 'tie_post_gallery', 10, 2 );
function tie_post_gallery( $output, $attr) {
    global $post, $wp_locale;

    static $instance = 0;
    $instance++;

    if ( isset( $attr['orderby'] ) ) {
        $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
        if ( !$attr['orderby'] )
            unset( $attr['orderby'] );
    }

    extract(shortcode_atts(array(
        'order'      => 'ASC',
        'orderby'    => 'menu_order ID',
        'id'         => $post->ID,
        'itemtag'    => 'dl',
        'icontag'    => 'dt',
        'captiontag' => 'dd',
        'columns'    => 3,
        'size'       => 'thumbnail',
        'include'    => '',
        'exclude'    => ''
    ), $attr));

    $id = intval($id);
    if ( 'RAND' == $order )
        $orderby = 'none';

    if ( !empty($include) ) {
        $include = preg_replace( '/[^0-9,]+/', '', $include );
        $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

        $attachments = array();
        foreach ( $_attachments as $key => $val ) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    } elseif ( !empty($exclude) ) {
        $exclude = preg_replace( '/[^0-9,]+/', '', $exclude );
        $attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
    } else {
        $attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
    }

    if ( empty($attachments) )
        return '';

    if ( is_feed() ) {
        $output = "\n";
        foreach ( $attachments as $att_id => $attachment )
            $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
        return $output;
    }

    $itemtag = tag_escape($itemtag);
    $captiontag = tag_escape($captiontag);
    $columns = intval($columns);
    $itemwidth = $columns > 0 ? floor(100/$columns) : 100;
    $float = is_rtl() ? 'right' : 'left';

    $selector = "gallery-{$instance}";
	
	$images_class ='';
	if( isset($attr['link']) && 'file' == $attr['link'] )
		$images_class = "gallery-images";
	
    $output = apply_filters('gallery_style', "
        <style type='text/css'>
            #{$selector} {
                margin: auto;
            }
            #{$selector} .gallery-item {
                float: {$float};
                margin-top: 10px;
                text-align: center;
                width: {$itemwidth}%;           }
            #{$selector} img {
                border: 2px solid #cfcfcf;
            }
            #{$selector} .gallery-caption {
                margin-left: 0;
            }
        </style>
        <!-- see gallery_shortcode() in wp-includes/media.php -->
        <div id='$selector' class='$images_class gallery galleryid-{$id}'>");

    $i = 0;
    foreach ( $attachments as $id => $attachment ) {
        $link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_link($id, $size, false, false) : wp_get_attachment_link($id, $size, true, false);

        $output .= "<{$itemtag} class='gallery-item'>";
        $output .= "
            <{$icontag} class='gallery-icon'>
                $link
            </{$icontag}>";
        if ( $captiontag && trim($attachment->post_excerpt) ) {
            $output .= "
                <{$captiontag} class='gallery-caption'>
                " . wptexturize($attachment->post_excerpt) . "
                </{$captiontag}>";
        }
        $output .= "</{$itemtag}>";
        if ( $columns > 0 && ++$i % $columns == 0 )
            $output .= '<br style="clear: both" />';
    }

    $output .= "
            <br style='clear: both;' />
        </div>\n";

    return $output;
}
	
	
/*-----------------------------------------------------------------------------------*/
/*-----------------------------------------------------------------------------------*/
function tie_fix_shortcodes($content){   
    $array = array (
		'[raw]' => '', 
        '[/raw]' => '', 
        '<p>[raw]' => '', 
        '[/raw]</p>' => '', 
        '[/raw]<br />' => '', 
        '<p>[' => '[', 
        ']</p>' => ']', 
        ']<br />' => ']'
    );

    $content = strtr($content, $array);
    return $content;
}
add_filter('the_content', 'tie_fix_shortcodes');


/*-----------------------------------------------------------------------------------*/
# Creates a nicely formatted and more specific title element text for output
/*-----------------------------------------------------------------------------------*/
function tie_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'tie' ), max( $paged, $page ) );

	return $title;
}
if ( !class_exists( 'All_in_One_SEO_Pack' ) && !function_exists( 'wpseo_get_value' ) ) 
add_filter( 'wp_title', 'tie_wp_title', 10, 2 );



/*-----------------------------------------------------------------------------------*/
# Chat Post Format
/*-----------------------------------------------------------------------------------*/
add_filter( 'the_content', 'tie_format_chat_content' );
add_filter( 'tie_post_format_chat_text', 'wpautop' );
function tie_format_chat_content( $content ) {
	global $_post_format_chat_ids;

	if ( !has_post_format( 'chat' ) )
		return $content;

	$_post_format_chat_ids = array();

	$separator = apply_filters( 'tie_post_format_chat_separator', ':' );

	$chat_output = "\n\t\t\t" . '<div id="chat-transcript-' . esc_attr( get_the_ID() ) . '" class="chat-transcript">';

	$chat_rows = preg_split( "/(\r?\n)+|(<br\s*\/?>\s*)+/", $content );

	foreach ( $chat_rows as $chat_row ) {

		if ( strpos( $chat_row, $separator ) ) {

			$chat_row_split = explode( $separator, trim( $chat_row ), 2 );

			$chat_author = strip_tags( trim( $chat_row_split[0] ) );

			$chat_text = trim( $chat_row_split[1] );

			$speaker_id = tie_format_chat_row_id( $chat_author );

			$chat_output .= "\n\t\t\t\t" . '<div class="chat-row ' . sanitize_html_class( "chat-speaker-{$speaker_id}" ) . '">';
			$chat_output .= "\n\t\t\t\t\t" . '<div class="chat-author ' . sanitize_html_class( strtolower( "chat-author-{$chat_author}" ) ) . ' vcard"><cite class="fn">' . apply_filters( 'tie_post_format_chat_author', $chat_author, $speaker_id ) . '</cite>' . /*$separator .*/ '</div>';
			$chat_output .= "\n\t\t\t\t\t" . '<div class="chat-text">' . str_replace( array( "\r", "\n", "\t" ), '', apply_filters( 'tie_post_format_chat_text', $chat_text, $chat_author, $speaker_id ) ) . '</div>';
			$chat_output .= "\n\t\t\t\t" . '</div><!-- .chat-row -->';
		}

		else {
			if ( !empty( $chat_row ) ) {

				$chat_output .= "\n\t\t\t\t" . '<div class="chat-row ' . sanitize_html_class( "chat-speaker-{$speaker_id}" ) . '">';
				$chat_output .= "\n\t\t\t\t\t" . '<div class="chat-text">' . str_replace( array( "\r", "\n", "\t" ), '', apply_filters( 'tie_post_format_chat_text', $chat_row, $chat_author, $speaker_id ) ) . '</div>';
				$chat_output .= "\n\t\t\t</div><!-- .chat-row -->";
			}
		}
	}
	$chat_output .= "\n\t\t\t</div><!-- .chat-transcript -->\n";
	return apply_filters( 'tie_post_format_chat_content', $chat_output );
}

function tie_format_chat_row_id( $chat_author ) {
	global $_post_format_chat_ids;
	$chat_author = strtolower( strip_tags( $chat_author ) );
	$_post_format_chat_ids[] = $chat_author;
	$_post_format_chat_ids = array_unique( $_post_format_chat_ids );
	return absint( array_search( $chat_author, $_post_format_chat_ids ) ) + 1;
}


/*-----------------------------------------------------------------------------------*/
# Posts Custom Colors Classes
/*-----------------------------------------------------------------------------------*/
function tie_custom_color_post_class($classes) {
	global $post;
	$get_meta = get_post_custom($post->ID);
	
	if( !empty( $get_meta["tie_post_color"][0] ) )
		$tie_post_color = $get_meta["tie_post_color"][0];
	
	if( empty($tie_post_color) ){
		$category = get_the_category($post->ID);
		$cat_id = $category[0]->cat_ID;
		
		if( !empty( $cat_id ) ){
			$cat_option = get_option('tie_cat_'.$cat_id);
			if( !empty($cat_option['tie_post_color']) )
				$tie_post_color = $cat_option['tie_post_color'];
		}
	}
	if( !empty( $tie_post_color ) )
		$classes[] = $tie_post_color;
		
	return $classes;
}
add_filter('post_class', 'tie_custom_color_post_class');

function tie_custom_color_body_class($classes) {
	global $post;
	if( is_singular() ){
		$get_meta = get_post_custom($post->ID);
		
		if( !empty( $get_meta["tie_post_color"][0] ) )
			$tie_post_color = $get_meta["tie_post_color"][0];
		
		if( empty($tie_post_color) && is_single() ){
			$category = get_the_category($post->ID);
			$cat_id = $category[0]->cat_ID;
			$cat_option = get_option('tie_cat_'.$cat_id);
			if( !empty($cat_option['tie_post_color']) )
				$tie_post_color = $cat_option['tie_post_color'];
		}
		if( !empty( $tie_post_color ) )
			$classes[] = $tie_post_color;
	}	
	return $classes;
}
add_filter('body_class', 'tie_custom_color_body_class');
	

/*-----------------------------------------------------------------------------------*/
# Custom COmments
/*-----------------------------------------------------------------------------------*/
function tie_custom_comments( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment ;
	?>
	<li id="comment-<?php comment_ID(); ?>">
		<div  <?php comment_class('comment-wrap'); ?> >
			<div class="comment-avatar"><?php echo get_avatar( $comment, 65 ); ?></div>

			<div class="comment-content">
				<div class="author-comment">
					<?php printf( __( '%s ', 'tie' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
					<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">	<?php printf( __( '%1$s at %2$s', 'tie' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'tie' ), ' ' ); ?></div><!-- .comment-meta .commentmetadata -->
					<div class="clear"></div>
				</div>
			
				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'tie' ); ?></em>
					<br />
				<?php endif; ?>
					
				<?php comment_text(); ?>
			</div>
			<div class="reply"><?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?></div><!-- .reply -->
		</div><!-- #comment-##  -->

	<?php
}
function tie_custom_pings($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment; ?>
	<li class="comment pingback">
		<p><?php _e( 'Pingback:', 'tie' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', 'tie' ), ' ' ); ?></p>
<?php	
}

	
/*-----------------------------------------------------------------------------------*/
# WP 3.6.0
/*-----------------------------------------------------------------------------------*/
// For old theme versions Video shortcode
function tie_video_fix_shortcodes($content){   
	$v = '/(\[(video)\s?.*?\])(.+?)(\[(\/video)\])/';
	$content = preg_replace( $v , '[embed]$3[/embed]' , $content);
    return $content;
}
add_filter('the_content', 'tie_video_fix_shortcodes', 0);

//To prevent wordpress from importing mediaelement css file
function tie_audio_video_shortcode(){
	if( !is_admin()){
		wp_enqueue_script( 'wp-mediaelement' );
		return false;
	}
}
add_filter('wp_audio_shortcode_library', 'tie_audio_video_shortcode');
add_filter('wp_video_shortcode_library', 'tie_audio_video_shortcode');

//Responsive Videos
function tie_video_width_shortcode( $html ){
	$width1 = 'width: 100%';
	return preg_replace('/width: ([0-9]*)px/',$width1,$html);
}
add_filter('wp_video_shortcode', 'tie_video_width_shortcode');


/*-----------------------------------------------------------------------------------*/
# Widgets
/*-----------------------------------------------------------------------------------*/
add_action( 'widgets_init', 'tie_widgets_init' );
function tie_widgets_init() {
	$before_widget =  '<div id="%1$s" class="widget %2$s">';
	$after_widget  =  '</div><!-- .widget /-->';
	$before_title  =  '<h4 class="widget-top">';
	$after_title   =  '</h4>
						';
	
	register_sidebar( array(
		'name' =>  __( 'Slide Out Widget Area', 'tie' ),
		'id' => 'slide-widget-area',
		'description' => __( 'Slide Out Widget Area', 'tie' ),
		'before_widget' => $before_widget , 'after_widget' => $after_widget , 'before_title' => $before_title , 'after_title' => $after_title ,
	) );
	

}

?>