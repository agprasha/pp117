<!DOCTYPE html>
<html <?php language_attributes(); ?> prefix="og: http://ogp.me/ns#">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
<?php wp_head(); ?>
</head>
<body id="top" <?php body_class(); ?>>
<div id="wrapper">
	<aside id="sidebar">
		<div class="cover-img">
			<div class="cover-body">
			<?php
				$logo_margin_top = $logo_margin_bottom = '';
				if( tie_get_option( 'logo_margin_top' ))
					$logo_margin_top = ' margin-top:'.tie_get_option( 'logo_margin_top' ).'px;';
				if( tie_get_option( 'logo_margin_bottom' ))
					$logo_margin_bottom = ' margin-bottom:'.tie_get_option( 'logo_margin_bottom' ).'px;';
				?>
				<div class="logo" style="<?php echo $logo_margin_top.$logo_margin_bottom ?>">
	<?php if( tie_get_option('logo_setting') == 'title' ): ?>
					<h2 id="site-title"><a href="<?php echo home_url() ?>/"><?php bloginfo('name'); ?></a></h2>
					<span><?php bloginfo( 'description' ); ?></span>
	<?php else : ?>
					<?php if( tie_get_option( 'logo' ) ) $logo = tie_get_option( 'logo' );
							else $logo = get_template_directory_uri().'/images/logo.png';
					?>
					<a title="<?php bloginfo('name'); ?>" href="<?php echo home_url(); ?>/">
						<img src="<?php echo $logo ; ?>" alt="<?php bloginfo('name'); ?>" />
					</a>
		<?php if( tie_get_option( 'logo_retina' ) && tie_get_option( 'logo_retina_width' ) && tie_get_option( 'logo_retina_height' )): ?>
		<script type="text/javascript">
		jQuery(document).ready(function($) {
			var retina = window.devicePixelRatio > 1 ? true : false;
			if(retina) {
				jQuery('#sidebar .logo img').attr('src', '<?php echo tie_get_option( 'logo_retina' ); ?>');
				jQuery('#sidebar .logo img').attr('width', '<?php echo tie_get_option( 'logo_retina_width' ); ?>');
				jQuery('#sidebar .logo img').attr('height', '<?php echo tie_get_option( 'logo_retina_height' ); ?>');
			}
		});
		</script>
		<?php endif; ?>
	<?php endif; ?>
				</div><!-- .logo /-->
			<?php
			//UberMenu Support
			$navID = 'main-nav';
			if ( class_exists( 'UberMenu' ) ){
				$uberMenus = get_option( 'wp-mega-menu-nav-locations' );
				if( !empty($uberMenus) && is_array($uberMenus) && in_array("primary", $uberMenus)) $navID = 'main-nav-uber';
			}?>
				<nav id="<?php echo $navID; ?>">
					<?php wp_nav_menu( array( 'container_class' => 'main-menu', 'theme_location' => 'primary'  ) ); ?>
				</nav><!-- .main-nav /-->
				
			<?php if( tie_get_option( 'about_author' ) ): ?>	
				<div class="about-blog-author">
					<?php if( tie_get_option( 'avatar_image' ) ): ?>	
					<span class="blog-author-avatar">
						<img src="<?php echo tie_get_option( 'avatar_image' ) ?>" title="<?php tie_get_option( 'author_about_name' ) ?>" alt="<?php tie_get_option( 'author_about_name' ) ?>" />
					</span>
					<?php endif; ?>
					<div class="blog-author-content">
						<h2 class="blog-author-name"><?php echo tie_get_option( 'author_about_name' ) ?></h2>
						<div><?php echo htmlspecialchars_decode(tie_get_option( 'author_about_text' )) ?></div>
						<?php if( tie_get_option( 'social_icons_sidebar' ) ) tie_get_social( 'yes' , 24 , '' );	?>
					</div>
				</div> <!-- .about-blog-author -->				
				<?php if( tie_get_option( 'avatar_type' ) == 'multi' ): ?>
				<span style="display:none;">
					<img src="<?php echo tie_get_option( 'avatar_image_top' ) ?>" alt="" />
					<img src="<?php echo tie_get_option( 'avatar_image_topright' ) ?>" alt="" />
					<img src="<?php echo tie_get_option( 'avatar_image_topleft' ) ?>" alt="" />
					<img src="<?php echo tie_get_option( 'avatar_image_bottom' ) ?>" alt="" />
					<img src="<?php echo tie_get_option( 'avatar_image_bottomleft' ) ?>" alt="" />
					<img src="<?php echo tie_get_option( 'avatar_image_bottomright' ) ?>" alt="" />
					<img src="<?php echo tie_get_option( 'avatar_image_left' ) ?>" alt="" />
					<img src="<?php echo tie_get_option( 'avatar_image_right' ) ?>" alt="" />
				</span>
				<script type='text/javascript'>
				jQuery( '#sidebar' ).mousemove(function(event){
					var tieX = event.pageX, tieY = event.pageY, tiePosition = jQuery( '.blog-author-avatar' ).offset();
					if( tieY < tiePosition.top && tieX > tiePosition.left && tieX < (tiePosition.left+100) ) //top
						jQuery(".blog-author-avatar img").attr("src", '<?php echo tie_get_option( 'avatar_image_top' ) ?>');
					else if( tieY > ( tiePosition.top+100 ) && tieX > tiePosition.left && tieX < (tiePosition.left+100) ) //bottom
						jQuery(".blog-author-avatar img").attr("src", '<?php echo tie_get_option( 'avatar_image_bottom' ) ?>');
					else if( tieY > (tiePosition.top+100) && tieX < tiePosition.left ) //bottom left
						jQuery(".blog-author-avatar img").attr("src", '<?php echo tie_get_option( 'avatar_image_bottomleft' ) ?>');
					else if( tieY < tiePosition.top && tieX < tiePosition.left ) //top left
						jQuery(".blog-author-avatar img").attr("src", '<?php echo tie_get_option( 'avatar_image_topleft' ) ?>');	
					else if( tieY > (tiePosition.top+100) && tieX > (tiePosition.left+100) ) //bottom right
						jQuery(".blog-author-avatar img").attr("src", '<?php echo tie_get_option( 'avatar_image_bottomright' ) ?>');
					else if( tieY < tiePosition.top && tieX > (tiePosition.left+100) ) //top right
						jQuery(".blog-author-avatar img").attr("src", '<?php echo tie_get_option( 'avatar_image_topright' ) ?>');	
					else if( tieX < tiePosition.left && tieY > tiePosition.top ) //left
						jQuery(".blog-author-avatar img").attr("src", '<?php echo tie_get_option( 'avatar_image_left' ) ?>');
					else if( tieX > ( tiePosition.left+100 ) )
						jQuery(".blog-author-avatar img").attr("src", '<?php echo tie_get_option( 'avatar_image_right' ) ?>');
					else //center
						jQuery(".blog-author-avatar img").attr("src", '<?php echo tie_get_option( 'avatar_image' ) ?>');
				});
				jQuery( '#sidebar' ).mouseout(function(){
					jQuery(".blog-author-avatar img").attr("src", '<?php echo tie_get_option( 'avatar_image' ) ?>'); 
				});
				jQuery( '#slide-out-open' ).mouseenter(function(){
					jQuery(".blog-author-avatar img").attr("src", '<?php echo tie_get_option( 'avatar_image_topleft' ) ?>'); 
				});
				</script>
				<?php endif; ?>
			<?php endif; ?>
			</div> <!-- .cover-body -->
		</div> <!-- .cover-img -->
	</aside><!-- #sidebar /-->
	<?php 
	$slide_out = '';
	if( !tie_get_option('slide_out_sidebar') ) $slide_out = ' class="slide-out-disable"'; ?>
	<a id="slide-out-open"<?php echo $slide_out ?> href="#"><i class="tieicon-menu"></i></a>
	<aside id="slide-out"<?php echo $slide_out ?>>
		<div id="mobile-menu"></div>
		<?php if( tie_get_option('slide_out_sidebar') ): ?>
			<?php dynamic_sidebar( 'slide-widget-area' ); ?>
		<?php endif; ?>
	</aside><!-- #slide-out /-->
	<?php 
		$animated_content = '';
		if( tie_get_option('animated_content') ) $animated_content =' animated';
	?>
	<div id="main-content" class="container<?php echo $animated_content; ?>">