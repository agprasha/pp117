<?php

add_action( 'widgets_init', 'tie_google_widget_box' );
function tie_google_widget_box() {
	register_widget( 'tie_google_widget' );
}
class tie_google_widget extends WP_Widget {

	function tie_google_widget() {
		$widget_ops = array( 'classname' => 'google-widget'  );
		$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'google-widget' );
		$this->WP_Widget( 'google-widget',theme_name .' - Google + page', $widget_ops, $control_ops );
	}
	
	function widget( $args, $instance ) {
		extract( $args );

		$title = apply_filters('widget_title', $instance['title'] );
		$page_url = $instance['page_url'];
		$google_type = $instance['google_type'];
		

		echo $before_widget;
		if ( $title )
			echo $before_title;
			echo $title ; ?>
		<?php echo $after_title; ?>
			<div class="google-box">
			<?php if( $google_type == 'author' ): ?>
				<div class="g-person" data-width="220" data-href="<?php echo $page_url ?>" data-rel="author"></div>
			<?php else: ?>
			<div class="g-page" data-width="220" data-href="<?php echo $page_url ?>" data-rel="publisher"></div>
			<?php endif; ?>
				<script type="text/javascript">
				  (function() {
					var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
					po.src = 'https://apis.google.com/js/platform.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
				  })();
				</script>
			</div>
	<?php 
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['page_url'] = strip_tags( $new_instance['page_url'] );
		$instance['google_type'] = strip_tags( $new_instance['google_type'] );
		return $instance;
	}

	function form( $instance ) {
		$defaults = array( 'title' =>__( 'Follow us on Google+' , 'tie') );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>">Title : </label>
			<input id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" class="widefat" type="text" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'page_url' ); ?>">Page Url : </label>
			<input id="<?php echo $this->get_field_id( 'page_url' ); ?>" name="<?php echo $this->get_field_name( 'page_url' ); ?>" value="<?php echo $instance['page_url']; ?>" class="widefat" type="text" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'google_type' ); ?>">Type : </label>
			<select id="<?php echo $this->get_field_id( 'google_type' ); ?>" name="<?php echo $this->get_field_name( 'google_type' ); ?>" >
				<option value="page" <?php if( $instance['google_type'] == 'page' ) echo "selected=\"selected\""; else echo ""; ?>>Page</option>
				<option value="author" <?php if( $instance['google_type'] == 'author' ) echo "selected=\"selected\""; else echo ""; ?>>Author Profile</option>
			</select>
		</p>

	<?php
	}
}
?>