<?php
add_action( 'widgets_init', 'tie_search_widget' );
function tie_search_widget() {
	register_widget( 'tie_search' );
}
class tie_search extends WP_Widget {
	function tie_search() {
		$widget_ops = array( 'classname' => 'search'  );
		$control_ops = array( 'width' => 250, 'height' => 350, 'id_base' => 'search-widget' );
		$this->WP_Widget( 'search-widget',theme_name .' - Search', $widget_ops, $control_ops );
	}
	function widget( $args, $instance ) {
		extract( $args );
		echo $before_widget;
	?>
		<div class="search-block">
			<form method="get" id="searchform" action="<?php echo home_url(); ?>/">
				<button class="search-button" type="submit" value="<?php __( 'Search' , 'tie' ) ?>"></button>
				<input type="text" id="s" name="s" value="<?php _e( 'Search...' , 'tie' ) ?>" onfocus="if (this.value == '<?php _e( 'Search...' , 'tie' ) ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php _e( 'Search...' , 'tie' ) ?>';}"  />
			</form>
		</div><!-- .search-block /-->
<?php
		echo $after_widget;
	}
}
?>