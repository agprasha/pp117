<?php if ( ! have_posts() ) : ?>
	<div id="post-0" class="post not-found item-list">
		<h2 class="entry-title"><?php _e( 'Not Found', 'tie' ); ?></h2>
		<div class="entry">
			<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'tie' ); ?></p>
			<?php get_search_form(); ?>
		</div>
	</div>

<?php else : ?>
<?php while ( have_posts() ) : the_post();?>
	<article <?php post_class('item-list') ?>>
		<div class="post-format-icon">
			<a href="<?php the_permalink(); ?>" class="item-date"><span><?php the_time('j') ?></span><small><?php the_time('M') ?></small></a>
		</div>
		<div class="post-inner">			
			<?php 
				$format = get_post_format();
				if( false === $format ) { $format = 'standard'; }
			?>
			 <?php get_template_part( 'content', $format ); ?>			
		</div>
	</article><!-- .item-list -->
<?php endwhile;?>
<?php endif; ?>