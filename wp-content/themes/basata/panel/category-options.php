<?php

add_action ( 'edit_category_form_fields', 'tie_category_fields');
function tie_category_fields( $tag ) {    //check for existing featured ID
    $cat_id = $tag->term_id;
	$cat_option = get_option('tie_cat_'.$cat_id);

    wp_enqueue_media();

?>
<tr class="form-field">
	<td colspan="2">
		<div class="tiepanel-item">
			<h3><?php echo theme_name ?> - Category Style </h3>
			<div class="option-item">
				<span class="label">Custom Sidebar BG</span>
				<input id="tie_post_bg" class="img-path" type="text" size="56" style="direction:ltr; text-laign:left" name="tie_cat[tie_post_bg]" value="<?php echo  $cat_option[ 'tie_post_bg' ]; ?>" />
				<input id="upload_tie_post_bg_button" type="button" class="small_button" value="Upload" />
					
				<div id="tie_post_bg-preview" class="img-preview" <?php if(! $cat_option[ 'tie_post_bg' ] ) echo 'style="display:none;"' ?>>
					<img src="<?php if( $cat_option[ 'tie_post_bg' ] ) echo $cat_option[ 'tie_post_bg' ] ; else echo get_template_directory_uri().'/panel/images/spacer.png'; ?>" alt="" />
					<a class="del-img" title="Delete"></a>
				</div>
			</div>	
			<script type='text/javascript'>
				jQuery('#tie_post_bg').change(function(){
					jQuery('#tie_post_bg-preview').show();
					jQuery('#tie_post_bg-preview img').attr("src", jQuery(this).val());
				});
				tie_set_uploader( 'tie_post_bg' );
			</script>
			<div class="option-item">
				<span class="label" style="padding-top:10px;">Custom Post Color</span>
				<?php
					$tie_post_color = $cat_option[ 'tie_post_color' ];
					$checked = 'checked="checked"';
				?>
				<ul id="post-color-options" class="tie-options">
					<li>
						<input id="tie_post_color" name="tie_cat[tie_post_color]" type="radio" value="" <?php if($tie_post_color == '' || !$tie_post_color ) echo $checked; ?> />
						<a class="checkbox-select" title="Default" href="#"><span style="background:#fff;">X</span></a>
					</li>
					<?php for( $i=1; $i<11; $i++){ ?>
					<li>
						<input id="tie_post_color"  name="tie_cat[tie_post_color]" type="radio" value="custom_color_<?php echo $i; ?>" <?php if($tie_post_color == 'custom_color_'.$i ) echo $checked; ?> />
						<a class="checkbox-select" href="#"><span style="background:<?php echo tie_get_option( 'custom_color_'.$i ); ?>"></span></a>
					</li>
					<?php } ?>
				</ul>
			</div>		
		</div>				
	</td>
</tr>
<?php
}


add_action ( 'edited_category', 'tie_save_extra_category_fileds');
function tie_save_extra_category_fileds( $term_id ) {
	$cat_id = $term_id;
	update_option( "tie_cat_$cat_id", $_POST["tie_cat"] );
}

?>