<?php


function panel_options() { 

	$categories_obj = get_categories('hide_empty=0');
	$categories = array();
	foreach ($categories_obj as $pn_cat) {
		$categories[$pn_cat->cat_ID] = $pn_cat->cat_name;
	}
	
	$sliders = array();
	$custom_slider = new WP_Query( array( 'post_type' => 'tie_slider', 'no_found_rows'=> 1, 'posts_per_page' => -1 ) );
	while ( $custom_slider->have_posts() ) {
		$custom_slider->the_post();
		$sliders[get_the_ID()] = get_the_title();
	}
	
	
$save='
	<div class="mpanel-submit">
		<input type="hidden" name="action" value="test_theme_data_save" />
        <input type="hidden" name="security" value="'. wp_create_nonce("test-theme-data").'" />
		<input name="save" class="mpanel-save" type="submit" value="Save Changes" />    
	</div>'; 
?>
		
		
<div id="save-alert"></div>

<div class="mo-panel">

	<div class="mo-panel-tabs">
		<div class="logo"></div>
		<ul>
			<li class="tie-tabs general"><a href="#tab1"><span></span>General Settings</a></li>
			<li class="tie-tabs header"><a href="#tab9"><span></span>Sidebars Settings</a></li>
			<li class="tie-tabs article"><a href="#tab6"><span></span>Article Settings</a></li>
			<li class="tie-tabs styling"><a href="#tab13"><span></span>Styling</a></li>
			<li class="tie-tabs typography"><a href="#tab14"><span></span>Typography</a></li>
			<li class="tie-tabs Social"><a href="#tab4"><span></span>Social Networking</a></li>
			<li class="tie-tabs advanced"><a href="#tab10"><span></span>Advanced</a></li>
			<li class="tie-tabs tie-rate tie-not-tab"><a target="_blank" href="http://themeforest.net/downloads?ref=TieLabs"><span></span>Rate <?php echo theme_name ?></a></li>
			<li class="tie-tabs tie-more tie-not-tab"><a target="_blank" href="http://themeforest.net/user/TieLabs/portfolio?ref=TieLabs"><span></span>More Themes</a></li>
		</ul>
		<div class="clear"></div>
	</div> <!-- .mo-panel-tabs -->
	
	
	<div class="mo-panel-content">
	<form action="/" name="tie_form" id="tie_form">

	
		<div id="tab1" class="tabs-wrap">
			<h2>General Settings</h2> <?php echo $save ?>
				
			<div class="tiepanel-item">
				<h3>Favicon</h3>
				<?php
					tie_options(
						array(	"name" => "Custom Favicon",
								"id" => "favicon",
								"type" => "upload"));
				?>
			</div>
			
			<div class="tiepanel-item">
				<h3>Custom Gravatar</h3>
				
				<?php
					tie_options(
						array(	"name" => "Custom Gravatar",
								"id" => "gravatar",
								"type" => "upload"));
				?>
			</div>	
	
			<div class="tiepanel-item">
				<h3>General Settings</h3>
				<?php
				
					tie_options(
						array(	"name" => "'Go To Top' Icon",
								"id" => "footer_top",
								"type" => "checkbox"));
					
					tie_options(
						array(	"name" => "Animated Load Content",
								"id" => "animated_content",
								"type" => "checkbox"));
								
					tie_options(
						array(	"name" => "LightBox Style",
								"id" => "lightbox_style",
								"type" => "select",
								"options" => array( "light_rounded"=>"Light Rounded" ,
													"light_square"=>"Light Square" ,
													"dark_square"=>"Dark Square" ,
													"dark_rounded"=>"Dark Rounded" ,
													"pp_default"=>"Pretty Style" ,
													"facebook"=>"Facebook" )));
				?>
			</div>
			
			
						
			<div class="tiepanel-item">
				<h3>Header Code</h3>
				<div class="option-item">
					<small>The following code will add to the &lt;head&gt; tag. Useful if you need to add additional scripts such as CSS or JS.</small>
					<textarea id="header_code" name="tie_options[header_code]" style="width:100%" rows="7"><?php echo htmlspecialchars_decode(tie_get_option('header_code'));  ?></textarea>				
				</div>
			</div>
			
			<div class="tiepanel-item">
				<h3>Footer Code</h3>
				<div class="option-item">
					<small>The following code will add to the footer before the closing  &lt;/body&gt; tag. Useful if you need to Javascript or tracking code.</small>

					<textarea id="footer_code" name="tie_options[footer_code]" style="width:100%" rows="7"><?php echo htmlspecialchars_decode(tie_get_option('footer_code'));  ?></textarea>				
				</div>
			</div>	
			
			<div class="tiepanel-item">
				<h3>Copyright Text</h3>
				<div class="option-item">
					<textarea id="tie_footer_one" name="tie_options[footer_one]" style="width:100%" rows="4"><?php echo htmlspecialchars_decode(tie_get_option('footer_one'));  ?></textarea>				
				</div>
			</div>	
			
		</div>
	
		<div id="tab9" class="tabs-wrap">
			<h2>Left Sidebar Settings</h2> <?php echo $save ?>
			
			<div class="tiepanel-item">
				<h3>Logo</h3>
				<?php
					tie_options(
						array( 	"name" => "Logo Setting",
								"id" => "logo_setting",
								"type" => "radio",
								"options" => array( "logo"=>"Custom Image Logo" ,
													"title"=>"Display Site Title" )));

					tie_options(
						array(	"name" => "Logo Image",
								"id" => "logo",
								"help" => "Upload a logo image, or enter URL to an image if it is already uploaded. the theme default logo gets applied if the input field is left blank.",
								"type" => "upload",
								"extra_text" => 'Recommended size (MAX) : 190px x 60px')); 
								
					tie_options(
						array(	"name" => "Logo Image (Retina Version @2x)",
								"id" => "logo_retina",
								"type" => "upload",
								"extra_text" => 'Please choose an image file for the retina version of the logo. It should be 2x the size of main logo.')); 			
					
					tie_options(
						array(	"name" => "Standard Logo Width for Retina Logo",
								"id" => "logo_retina_width",
								"type" => "short-text",
								"extra_text" => 'If retina logo is uploaded, please enter the standard logo (1x) version width, do not enter the retina logo width.')); 			

					tie_options(
						array(	"name" => "Standard Logo Height for Retina Logo",
								"id" => "logo_retina_height",
								"type" => "short-text",
								"extra_text" => 'If retina logo is uploaded, please enter the standard logo (1x) version height, do not enter the retina logo height.')); 			
								
					tie_options(
						array(	"name" => "Logo Margin Top",
								"id" => "logo_margin_top",
								"type" => "slider",
								"help" => "Input number to set the top space of the logo .",
								"unit" => "px",
								"max" => 100,
								"min" => 0 ));
								
					tie_options(
						array(	"name" => "Logo Margin Bottom",
								"id" => "logo_margin_bottom",
								"type" => "slider",
								"help" => "Input number to set the Bottom space of the logo .",
								"unit" => "px",
								"max" => 100,
								"min" => 0 ));
			
				?>

			</div>
			
			<div class="tiepanel-item">
				<h3>About Author</h3>
				<?php
					tie_options(
						array(	"name" => "About Author",
								"id" => "about_author",
								"type" => "checkbox"));
								
					tie_options(
						array(	"name" => "Author Name",
								"id" => "author_about_name",
								"type" => "text"));	
								
					tie_options(
						array(	"name" => "About Text",
								"id" => "author_about_text",
								"type" => "textarea"));

					tie_options(
						array(	"name" => "Social Icons",
								"id" => "social_icons_sidebar",
								"type" => "checkbox"));
								
					tie_options(
						array( 	"name" => "Avatar Type",
								"id" => "avatar_type",
								"type" => "radio",
								"options" => array( "single"=>"Single Image" ,
													"multi"=>"Multiple Images - ( Change with mouse movements )" )));
				
					tie_options(
						array(	"name" => "Front Avatar Image",
								"id" => "avatar_image",
								"type" => "upload"));
					?>
				<div id="author-avatars">
					<?php
					tie_options(
						array(	"name" => "Top Avatar Image",
								"id" => "avatar_image_top",
								"type" => "upload"));
						
					tie_options(
						array(	"name" => "Top Right Avatar Image",
								"id" => "avatar_image_topright",
								"type" => "upload"));
						
					tie_options(
						array(	"name" => "Top Left Avatar Image",
								"id" => "avatar_image_topleft",
								"type" => "upload"));
						
					tie_options(
						array(	"name" => "Bottom Avatar Image",
								"id" => "avatar_image_bottom",
								"type" => "upload"));
						
					tie_options(
						array(	"name" => "Bottom Right Avatar Image",
								"id" => "avatar_image_bottomright",
								"type" => "upload"));
						
					tie_options(
						array(	"name" => "Bottom Left Avatar Image",
								"id" => "avatar_image_bottomleft",
								"type" => "upload"));
						
					tie_options(
						array(	"name" => "Right Avatar Image",
								"id" => "avatar_image_right",
								"type" => "upload"));
								
					tie_options(
						array(	"name" => "Left Avatar Image",
								"id" => "avatar_image_left",
								"type" => "upload"));
					?>
				</div>
			</div>

			<div class="tiepanel-item">
				<h3>Slide Out Sidebar</h3>
				<?php
					tie_options(
						array(	"name" => "Activate Slide Out Sidebar",
								"id" => "slide_out_sidebar",
								"type" => "checkbox"));
				?>
			</div>
						
		</div> <!-- Header Settings -->
		
	
		
		<div id="tab4" class="tabs-wrap">
			<h2>Social Networking</h2> <?php echo $save ?>

			<div class="tiepanel-item">
				<h3>Custom Feed URL</h3>
							
				<?php
					tie_options(
						array(	"name" => "Hide Rss Icon",
								"id" => "rss_icon",
								"type" => "checkbox"));
								
					tie_options(
						array(	"name" => "Custom Feed URL",
								"id" => "rss_url",
								"help" => "e.g. http://feedburner.com/userid",
								"type" => "text"));
				?>
			</div>
			
		<div class="tiepanel-item">
				<h3>Social Networking</h3>
				<p class="tie_message_hint"> Don't forget http:// before link .</p>
						
				<?php						
					tie_options(
						array(	"name" => "Facebook URL",
								"id" => "social",
								"key" => "facebook",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Twitter URL",
								"id" => "social",
								"key" => "twitter",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Google+ URL",
								"id" => "social",
								"key" => "google_plus",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "MySpace URL",
								"id" => "social",
								"key" => "myspace",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "dribbble URL",
								"id" => "social",
								"key" => "dribbble",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "LinkedIn URL",
								"id" => "social",
								"key" => "linkedin",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "evernote URL",
								"id" => "social",
								"key" => "evernote",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Dropbox URL",
								"id" => "social",
								"key" => "dropbox",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Flickr URL",
								"id" => "social",
								"key" => "flickr",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Picasa Web URL",
								"id" => "social",
								"key" => "picasa",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "DeviantArt URL",
								"id" => "social",
								"key" => "deviantart",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "YouTube URL",
								"id" => "social",
								"key" => "youtube",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "grooveshark URL",
								"id" => "social",
								"key" => "grooveshark",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Vimeo URL",
								"id" => "social",
								"key" => "vimeo",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "ShareThis URL",
								"id" => "social",
								"key" => "sharethis",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "yahoobuzz URL",
								"id" => "social",
								"key" => "yahoobuzz",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "500px URL",
								"id" => "social",
								"key" => "px500",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Skype URL",
								"id" => "social",
								"key" => "skype",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Digg URL",
								"id" => "social",
								"key" => "digg",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Reddit URL",
								"id" => "social",
								"key" => "reddit",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Delicious URL",
								"id" => "social",
								"key" => "delicious",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "StumbleUpon  URL",
								"key" => "stumbleupon",
								"id" => "social",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "FriendFeed URL",
								"id" => "social",
								"key" => "friendfeed",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Tumblr URL",
								"id" => "social",
								"key" => "tumblr",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Blogger URL",
								"id" => "social",
								"key" => "blogger",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Wordpress URL",
								"id" => "social",
								"key" => "wordpress",
								"type" => "arrayText"));						
					tie_options(
						array(	"name" => "Yelp URL",
								"id" => "social",
								"key" => "yelp",
								"type" => "arrayText"));							
					tie_options(
						array(	"name" => "posterous URL",
								"id" => "social",
								"key" => "posterous",
								"type" => "arrayText"));																														
					tie_options(
						array(	"name" => "Last.fm URL",
								"id" => "social",
								"key" => "lastfm",
								"type" => "arrayText"));						
					tie_options(
						array(	"name" => "Apple URL",
								"id" => "social",
								"key" => "apple",
								"type" => "arrayText"));											
					tie_options(
						array(	"name" => "FourSquare URL",
								"id" => "social",
								"key" => "foursquare",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Github URL",
								"id" => "social",
								"key" => "github",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "openid URL",
								"id" => "social",
								"key" => "openid",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "SoundCloud URL",
								"id" => "social",
								"key" => "soundcloud",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "xing.me URL",
								"id" => "social",
								"key" => "xing",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Google Play URL",
								"id" => "social",
								"key" => "google_play",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Pinterest URL",
								"id" => "social",
								"key" => "Pinterest",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Instagram URL",
								"id" => "social",
								"key" => "instagram",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Spotify URL",
								"id" => "social",
								"key" => "spotify",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "PayPal URL",
								"id" => "social",
								"key" => "paypal",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Forrst URL",
								"id" => "social",
								"key" => "forrst",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Behance URL",
								"id" => "social",
								"key" => "behance",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "Viadeo URL",
								"id" => "social",
								"key" => "viadeo",
								"type" => "arrayText"));
					tie_options(
						array(	"name" => "VK.com URL",
								"id" => "social",
								"key" => "vk",
								"type" => "arrayText"));
				?>
			</div>			
		</div><!-- Social Networking -->
		
		
		
				
		<div id="tab6" class="tab_content tabs-wrap">
			<h2>Article Settings</h2> <?php echo $save ?>
		
			<div class="tiepanel-item">

				<h3>Post Meta Settings</h3>
				<?php
					tie_options(
						array(	"name" => "Post Meta :",
								"id" => "post_meta",
								"type" => "checkbox"));

					tie_options(
						array(	"name" => "Author Meta",
								"id" => "post_author",
								"type" => "checkbox"));

					tie_options(
						array(	"name" => "Date Meta",
								"id" => "post_date",
								"type" => "checkbox"));


					tie_options(
						array(	"name" => "Categories Meta",
								"id" => "post_cats",
								"type" => "checkbox"));
								
					tie_options(
						array(	"name" => "Comments Meta",
								"id" => "post_comments",
								"type" => "checkbox"));


					tie_options(
						array(	"name" => "Tags Meta",
								"id" => "post_tags",
								"type" => "checkbox"));			
				?>	
			</div>
			
			<div class="tiepanel-item">

				<h3>Article Elements</h3>
				<?php

					tie_options(
						array(	"name" => "Post Author Box",
								"desc" => "",
								"id" => "post_authorbio",
								"type" => "checkbox"));

					tie_options(
						array(	"name" => "Next/Prev Article",
								"desc" => "",
								"id" => "post_nav",
								"type" => "checkbox")); 
								
					tie_options(
						array(	"name" => "Share Post Buttons",
								"id" => "share_post",
								"type" => "checkbox"));

				?>
			</div>
			
			<div class="tiepanel-item">

				<h3>Related Posts Settings</h3>
				<?php
					tie_options(
						array(	"name" => "Related Posts",
								"id" => "related",
								"type" => "checkbox")); 
								
					tie_options(
						array(	"name" => "Number of posts to show",
								"id" => "related_number",
								"type" => "short-text"));
								
					tie_options(
						array(	"name" => "Query Type",
								"id" => "related_query",
								"options" => array( "category"=>"Category" ,
													"tag"=>"Tag",
													"author"=>"Author" ),
								"type" => "radio")); 
				?>
			</div>

			
		</div> <!-- Article Settings -->
		
		
				
		<div id="tab13" class="tab_content tabs-wrap">
			<h2>Styling</h2>	<?php echo $save ?>	
			
			<div class="tiepanel-item">
				<h3>Body Styling</h3>
				<?php
					tie_options(
						array(	"name" => "Theme Color",
								"id" => "theme_color",
								"type" => "color"));

					tie_options(
						array(	"name" => "Links Color",
								"id" => "links_color",
								"type" => "color"));
								
					tie_options(
						array(	"name" => "Links Decoration",
								"id" => "links_decoration",
								"type" => "select",
								"options" => array( ""=>"Default" ,
													"none"=>"none",
													"underline"=>"underline",
													"overline"=>"overline",
													"line-through"=>"line-through" )));
					tie_options(
						array(	"name" => "Links Color on mouse over",
								"id" => "links_color_hover",
								"type" => "color"));

				
					tie_options(
						array(	"name" => "Links Decoration on mouse over",
								"id" => "links_decoration_hover",
								"type" => "select",
								"options" => array( ""=>"Default" ,
													"none"=>"none",
													"underline"=>"underline",
													"overline"=>"overline",
													"line-through"=>"line-through" )));
				?>
			</div>

			<div class="tiepanel-item">
				<h3>Main Sidebar Background</h3>
				<?php
					tie_options(
						array(	"name" => "Background",
								"id" => "main_sidebar_bg",
								"type" => "upload"));
								
				?>
			</div>
			
			<div class="tiepanel-item">
				<h3>Main Navigation Styling</h3>
				<?php
					tie_options(
						array(	"name" => "Background Color Opacity",
								"id" => "nav_bg_opacity",
								"type" => "slider",
								"help" => "Input number to set the top space of the logo .",
								"unit" => "%",
								"max" => 100,
								"min" => 0 ));
								
				?>
			</div>
			
			<div class="tiepanel-item">
				<h3>Post Styling</h3>
				<?php
					tie_options(
						array(	"name" => "Post Links Color",
								"id" => "post_links_color",
								"type" => "color"));
				
					tie_options(
						array(	"name" => "Post Links Decoration",
								"id" => "post_links_decoration",
								"type" => "select",
								"options" => array( ""=>"Default" ,
													"none"=>"none",
													"underline"=>"underline",
													"overline"=>"overline",
													"line-through"=>"line-through" )));
				
					tie_options(
						array(	"name" => "Post Links Color on mouse over",
								"id" => "post_links_color_hover",
								"type" => "color"));
				
					tie_options(
						array(	"name" => "Post Links Decoration on mouse over",
								"id" => "post_links_decoration_hover",
								"type" => "select",
								"options" => array( ""=>"Default" ,
													"none"=>"none",
													"underline"=>"underline",
													"overline"=>"overline",
													"line-through"=>"line-through" )));
				?>
			</div>
			<div class="tiepanel-item">
				<h3>Custom Post Colors</h3>
					<?php for( $i=1; $i<11; $i++){
					tie_options(
						array(	"name" => "Custom Post Color #$i",
								"id" => "custom_color_$i",
								"type" => "color"));
					} ?>
			</div>
						
			<div class="tiepanel-item">
				<h3>Custom CSS</h3>	
				<div class="option-item">
					<p><strong>Global CSS :</strong></p>
					<textarea id="tie_css" name="tie_options[css]" style="width:100%" rows="7"><?php echo tie_get_option('css');  ?></textarea>
				</div>	
				<div class="option-item">
					<p><strong>Tablets CSS :</strong> Width from 768px to 985px</p>
					<textarea id="tie_css" name="tie_options[css_tablets]" style="width:100%" rows="7"><?php echo tie_get_option('css_tablets');  ?></textarea>
				</div>
				<div class="option-item">
					<p><strong>Wide Phones CSS :</strong> Width from 480px to 767px</p>
					<textarea id="tie_css" name="tie_options[css_wide_phones]" style="width:100%" rows="7"><?php echo tie_get_option('css_wide_phones');  ?></textarea>
				</div>
				<div class="option-item">
					<p><strong>Phones CSS :</strong> Width from 320px to 479px</p>
					<textarea id="tie_css" name="tie_options[css_phones]" style="width:100%" rows="7"><?php echo tie_get_option('css_phones');  ?></textarea>
				</div>	
			</div>	

		</div> <!-- Styling -->

		<div id="tab14" class="tab_content tabs-wrap">
			<h2>Typography</h2>	<?php echo $save ?>	
			
			<div class="tiepanel-item">
				<h3>Character sets</h3>
				<p class="tie_message_hint"><strong>Tip:</strong> If you choose only the languages that you need, you'll help prevent slowness on your webpage.</p>
				<?php
					tie_options(
						array(	"name" => "Latin Extended",
								"id" => "typography_latin_extended",
								"type" => "checkbox"));

					tie_options(
						array(	"name" => "Cyrillic",
								"id" => "typography_cyrillic",
								"type" => "checkbox"));

					tie_options(
						array(	"name" => "Cyrillic Extended",
								"id" => "typography_cyrillic_extended",
								"type" => "checkbox"));
								
					tie_options(
						array(	"name" => "Greek",
								"id" => "typography_greek",
								"type" => "checkbox"));
								
					tie_options(
						array(	"name" => "Greek Extended",
								"id" => "typography_greek_extended",
								"type" => "checkbox"));	
								
					tie_options(
						array(	"name" => "Khmer",
								"id" => "typography_khmer",
								"type" => "checkbox"));		
								
					tie_options(
						array(	"name" => "Vietnamese",
								"id" => "typography_vietnamese",
								"type" => "checkbox"));
				?>
			</div>
			
			<div class="tiepanel-item">
				<h3>Live typography preview</h3>
					<?php 	global $options_fonts;
					tie_options(
						array( 	"name" => "",
								"id" => "typography_test",
								"type" => "typography"));
						?>
	
				<div id="font-preview" class="option-item">Grumpy wizards make toxic brew for the evil Queen and Jack.</div>		

			</div>
			
			<div class="tiepanel-item">
				<h3>Main Typography</h3>
				<?php
					tie_options(
						array( 	"name" => "General Typography",
								"id" => "typography_general",
								"type" => "typography"));
	
					tie_options(
						array( 	"name" => "Main navigation",
								"id" => "typography_main_nav",
								"type" => "typography"));

					tie_options(
						array( 	"name" => "Single Post Title",
								"id" => "typography_post_title",
								"type" => "typography"));

					tie_options(
						array( 	"name" => "Post Meta",
								"id" => "typography_post_meta",
								"type" => "typography"));

					tie_options(
						array( 	"name" => "Post Entry , page Entry",
								"id" => "typography_post_entry",
								"type" => "typography"));
								
					tie_options(
						array( 	"name" => "Widgets Titles",
								"id" => "typography_widgets_title",
								"type" => "typography"));
				?>
			</div>

			<div class="tiepanel-item">
				<h3>Post Headings</h3>
				<?php
					tie_options(
						array( 	"name" => "H1 Typography",
								"id" => "typography_post_h1",
								"type" => "typography"));
	
					tie_options(
						array( 	"name" => "H2 Typography",
								"id" => "typography_post_h2",
								"type" => "typography"));
	
					tie_options(
						array( 	"name" => "H3 Typography",
								"id" => "typography_post_h3",
								"type" => "typography"));
	
					tie_options(
						array( 	"name" => "H4 Typography",
								"id" => "typography_post_h4",
								"type" => "typography"));
	
					tie_options(
						array( 	"name" => "H5 Typography",
								"id" => "typography_post_h5",
								"type" => "typography"));
	
					tie_options(
						array( 	"name" => "H6 Typography",
								"id" => "typography_post_h6",
								"type" => "typography"));
	
				?>
			</div>
			
		</div> <!-- Typography -->
		
		
		<div id="tab10" class="tab_content tabs-wrap">
			<h2>Advanced Settings</h2>	<?php echo $save ?>	
				
			<div class="tiepanel-item">
				<h3>Theme Updates</h3>
				<?php
					tie_options(
						array(	"name" => "Notify On Theme Updates",
								"id" => "notify_theme",
								"type" => "checkbox"));
				?>
			</div>

			<div class="tiepanel-item">
				<h3>Wordpress Login page Logo</h3>
				<?php
					tie_options(
						array(	"name" => "Worpress Login page Logo",
								"id" => "dashboard_logo",
								"type" => "upload"));
				?>
			
			</div>
			
			<div class="tiepanel-item">
				<h3>Twitter API OAuth settings</h3>
				<p class="tie_message_hint">This information will uses in Sicail counter and Twitter Widget .. You need to create <a href="https://dev.twitter.com/apps" target="_blank">Twitter APP</a> to get this info .. check this <a href="https://vimeo.com/59573397" target="_blank">Video</a> .</p>

				<?php
					tie_options(
						array(	"name" => "Twitter Username",
								"id" => "twitter_username",
								"type" => "text"));

					tie_options(
						array(	"name" => "Consumer key",
								"id" => "twitter_consumer_key",
								"type" => "text"));
								
					tie_options(
						array(	"name" => "Consumer secret",
								"id" => "twitter_consumer_secret",
								"type" => "text"));	
								
					tie_options(
						array(	"name" => "Access token",
								"id" => "twitter_access_token",
								"type" => "text"));	
								
					tie_options(
						array(	"name" => "Access token secret",
								"id" => "twitter_access_token_secret",
								"type" => "text"));
				?>
			</div>	
			
			<?php
				global $array_options ;
				
				$current_options = array();
				foreach( $array_options as $option ){
					if( get_option( $option ) )
						$current_options[$option] =  get_option( $option ) ;
				}
			?>
			
			<div class="tiepanel-item">
				<h3>Export</h3>
				<div class="option-item">
					<textarea style="width:100%" rows="7"><?php echo $currentsettings = base64_encode( serialize( $current_options )); ?></textarea>
				</div>
			</div>
			<div class="tiepanel-item">
				<h3>Import</h3>
				<div class="option-item">
					<textarea id="tie_import" name="tie_import" style="width:100%" rows="7"></textarea>
				</div>
			</div>
	
		</div> <!-- Advanced -->
		
		
		<div class="mo-footer">
			<?php echo $save; ?>
		</form>

			<form method="post">
				<div class="mpanel-reset">
					<input type="hidden" name="resetnonce" value="<?php echo wp_create_nonce('reset-action-code'); ?>" />
					<input name="reset" class="mpanel-reset-button" type="submit" onClick="if(confirm('All settings will be rest .. Are you sure ?')) return true ; else return false; " value="Reset All Settings" />
					<input type="hidden" name="action" value="tiereset" />
				</div>
			</form>
		</div>

	</div><!-- .mo-panel-content -->
</div><!-- .mo-panel -->


<?php
}
?>