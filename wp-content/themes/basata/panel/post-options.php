<?php
add_action("admin_init", "tie_posts_init");
function tie_posts_init(){
	add_meta_box("tie_format_gallery_options", theme_name ." - Gallery Options", "tie_format_gallery_options", "post", "normal", "high");
	add_meta_box("tie_format_link_options", theme_name ." - Link Options", "tie_format_link_options", "post", "normal", "high");
	add_meta_box("tie_format_quote_options", theme_name ." - Quote Options", "tie_format_quote_options", "post", "normal", "high");
	add_meta_box("tie_format_video_options", theme_name ." - Video Options", "tie_format_video_options", "post", "normal", "high");
	add_meta_box("tie_format_audio_options", theme_name ." - Audio Options", "tie_format_audio_options", "post", "normal", "high");
	add_meta_box("tie_format_status_options", theme_name ." - Status Options", "tie_format_status_options", "post", "normal", "high");

	add_meta_box("tie_general_post_options", theme_name ." - Post Options", "tie_general_post_options", "post", "normal", "high");
	add_meta_box("tie_general_post_options", theme_name ." - Post Options", "tie_general_post_options", "page", "normal", "high");
}

function tie_format_gallery_options(){
	global $post ;
	
	$custom = get_post_custom($post->ID);
	$gallery = unserialize( $custom["post_gallery"][0] );
	
    wp_enqueue_media();

	tie_post_options(				
		array(	"name" => "Gallery Style",
				"id" => "tie_gallery_style",
				"type" => "select",
				"options" => array( "slider" => "Slider" ,
									"grid" => "Grid")));	
  ?>
  <script>
  jQuery(document).ready(function() {
  
	jQuery(function() {
		jQuery( "#tie-gallery-items" ).sortable({placeholder: "ui-state-highlight"});
	});
	// Uploading files
	var tie_uploader;
	jQuery(document).on("click", "#upload_add_slide" , function( event ){
 		event.preventDefault();
		tie_uploader = wp.media.frames.tie_uploader = wp.media({
			title: 'Insert Images | Press CTRL to Multi Select .',
			library: {type: 'image'},
			button: {text: 'Select'},
			multiple: true
		});

		tie_uploader.on( 'select', function() {
			var selection = tie_uploader.state().get('selection');
			selection.map( function( attachment ) {
				attachment = attachment.toJSON();
				jQuery('#tie-gallery-items').append('<li id="listItem_'+ nextCell +'" class="ui-state-default"><div class="gallery-img"><img src="'+attachment.url+'" alt=""><input id="custom_gallery['+ nextCell +'][id]" name="custom_gallery['+ nextCell +'][id]" value="'+attachment.id+'" type="hidden" /><a class="del-cat"></a></div></li>');
				nextCell ++ ;
			});
		});
		tie_uploader.open();
	});
	
	
	
});

</script>
   <input id="upload_add_slide" type="button" class="mpanel-save" value="Add New image">
	<ul id="tie-gallery-items">
	<?php
	if( $gallery ){
	$i=0;
	foreach( $gallery as $slide ):
		$i++; ?>
		<li id="listItem_<?php echo $i ?>"  class="ui-state-default">
			<div class="gallery-img"><?php echo wp_get_attachment_image( $slide['id'] , 'thumbnail' );  ?>
				<input id="custom_gallery[<?php echo $i ?>][id]" name="custom_gallery[<?php echo $i ?>][id]" value="<?php  echo $slide['id']  ?>" type="hidden" />
				<a class="del-cat"></a>
			</div>
		</li>
	<?php endforeach; 
	}else{
		//echo ' <br /> Use the button above to add images !';
	}?>
	</ul>
	<script> var nextCell = <?php echo $i+1 ?> ;</script>
	
<?php
}

function tie_format_link_options(){
	
	tie_post_options(				
		array(	"name" => "Link url",
				"id" => "tie_link_url",
				"type" => "text"));
				
	tie_post_options(				
		array(	"name" => "Link Description ",
				"id" => "tie_link_desc",
				"type" => "text"));
}

function tie_format_quote_options(){
	
	tie_post_options(				
		array(	"name" => "Quote Author",
				"id" => "tie_quote_author",
				"type" => "text"));

	tie_post_options(				
		array(	"name" => "Quote Author Link",
				"id" => "tie_quote_link",
				"type" => "text"));
				
	tie_post_options(				
		array(	"name" => "The Quote",
				"id" => "tie_quote_text",
				"type" => "textarea"));

}

function tie_format_status_options(){
	
	tie_post_options(				
		array(	"name" => "FaceBook Post URL",
				"id" => "tie_status_facebook",
				"type" => "text"));
	echo '<div class="option-item"><strong>OR</strong></div>';
	tie_post_options(				
		array(	"name" => "Tweet URL",
				"id" => "tie_status_twitter",
				"type" => "text"));
}

function tie_format_video_options(){
	tie_post_options(				
		array(	"name" => "Embed Code",
				"id" => "tie_embed_code",
				"type" => "textarea"));

	tie_post_options(				
		array(	"name" => "Youtube / Vimeo / Dailymotion Video Url",
				"id" => "tie_video_url",
				"type" => "text"));					
}

function tie_format_audio_options(){

	tie_post_options(				
			array(	"name" => "Mp3 file Url",
					"id" => "tie_audio_mp3",
					"type" => "text"));

		tie_post_options(				
			array(	"name" => "M4A file Url",
					"id" => "tie_audio_m4a",
					"type" => "text"));
					
					
		tie_post_options(				
			array(	"name" => "OGA file Url :",
					"id" => "tie_audio_oga",
					"type" => "text"));
					
		echo '<div class="option-item"><strong>OR</strong></div>';
		
		tie_post_options(				
			array(	"name" => "SoundCloud URL",
					"id" => "tie_audio_soundcloud",
					"type" => "text"));
}


function tie_general_post_options(){
	global $post ;
	$get_meta = get_post_custom($post->ID);
	$tie_post_color = $get_meta["tie_post_color"][0];
	
	tie_post_options(				
		array(	"name" => "Custom Sidebar BG",
				"id" => "tie_post_bg",
				"type" => "upload"
		));
		
		?>
			<div class="option-item">
				<span class="label" style="padding-top:10px;">Custom Post Color</span>
				<?php
					$checked = 'checked="checked"';
				?>
				<ul id="post-color-options" class="tie-options">
					<li>
						<input id="tie_post_color" name="tie_post_color" type="radio" value="" <?php if($tie_post_color == '' || !$tie_post_color ) echo $checked; ?> />
						<a class="checkbox-select" title="Default" href="#"><span style="background:#fff;">X</span></a>
					</li>
					<?php for( $i=1; $i<11; $i++){ ?>
					<li>
						<input id="tie_post_color"  name="tie_post_color" type="radio" value="custom_color_<?php echo $i; ?>" <?php if($tie_post_color == 'custom_color_'.$i ) echo $checked; ?> />
						<a class="checkbox-select" href="#"><span style="background:<?php echo tie_get_option( 'custom_color_'.$i ); ?>"></span></a>
					</li>
					<?php } ?>
				</ul>
			</div>
			<?php
}


add_action('save_post', 'tie_save_post');
function tie_save_post(){
	global $post;
	
	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
		return $post_id;

 	$custom_meta_fields = array(
		'tie_post_bg',
		'tie_post_color',
		'tie_gallery_style',
		'tie_link_url',
		'tie_link_desc',
		'tie_video_url',
		'tie_embed_code',
		'tie_audio_mp3',
		'tie_audio_m4a',
		'tie_audio_oga',
		'tie_audio_soundcloud',
		'tie_quote_author',
		'tie_quote_link',
		'tie_quote_text',
		"tie_status_facebook",
		"tie_status_twitter"
	);
	foreach( $custom_meta_fields as $custom_meta_field ){
	
		if(isset($_POST[$custom_meta_field]) )
			update_post_meta($post->ID, $custom_meta_field, htmlspecialchars(stripslashes($_POST[$custom_meta_field])) );
		else
			delete_post_meta($post->ID, $custom_meta_field);
	}
	
	if( $_POST['custom_gallery'] && $_POST['custom_gallery'] != "" ){
		update_post_meta($post->ID, 'post_gallery' , $_POST['custom_gallery']);		
	}
	else{ 
		delete_post_meta($post->ID, 'post_gallery' );
	}
}




/*********************************************************/

function tie_post_options($value){
	global $post;
?>

	<div class="option-item" id="<?php echo $value['id'] ?>-item">
		<span class="label"><?php  echo $value['name']; ?></span>
	<?php
		$id = $value['id'];
		$get_meta = get_post_custom($post->ID);
		
		if( isset( $get_meta[$id][0] ) )
			$current_value = $get_meta[$id][0];
			
	switch ( $value['type'] ) {
	
		case 'text': ?>
			<input  name="<?php echo $value['id']; ?>" id="<?php  echo $value['id']; ?>" type="text" value="<?php echo $current_value ?>" />
		<?php 
		break;

		case 'checkbox':
			if( !empty( $current_value ) ){$checked = "checked=\"checked\"";  } else{$checked = "";} ?>
				<input type="checkbox" name="<?php echo $value['id'] ?>" id="<?php echo $value['id'] ?>" value="true" <?php echo $checked; ?> />			
		<?php	
		break;
		
		case 'select':
		?>
			<select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">
				<?php foreach ($value['options'] as $key => $option) { ?>
				<option value="<?php echo $key ?>" <?php if ( $current_value == $key) { echo ' selected="selected"' ; } ?>><?php echo $option; ?></option>
				<?php } ?>
			</select>
		<?php
		break;
		
		case 'textarea':
		?>
			<textarea style="direction:ltr; text-align:left; width:430px;" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="textarea" cols="100%" rows="3" tabindex="4"><?php echo $current_value  ?></textarea>
		<?php
		break;
		
		case 'upload':
		?>
				<input id="<?php echo $value['id']; ?>" class="img-path" type="text" size="56" style="direction:ltr; text-laign:left" name="<?php echo $value['id']; ?>" value="<?php echo $current_value ?>" />
				<input id="upload_<?php echo $value['id']; ?>_button" type="button" class="small_button" value="Upload" />
				<div id="<?php echo $value['id']; ?>-preview" class="img-preview" <?php if( empty($current_value) ) echo 'style="display:none;"' ?>>
					<img src="<?php if( !empty( $current_value ) ) echo $current_value ; else echo get_template_directory_uri().'/panel/images/spacer.png'; ?>" alt="" />
					<a class="del-img" title="Delete"></a>
				</div>
				<script type='text/javascript'>
					jQuery('#<?php echo $value['id']; ?>').change(function(){
						jQuery('#<?php echo $value['id']; ?>-preview').show();
						jQuery('#<?php echo $value['id']; ?>-preview img').attr("src", jQuery(this).val());
					});
					tie_set_uploader( '<?php echo $value['id']; ?>' );
				</script>
		<?php
		break;
	} ?>
	</div>
<?php
}
?>