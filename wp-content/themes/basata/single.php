<?php get_header(); ?>
	<div id="content">
		<?php if ( ! have_posts() ) : ?>
		<div id="post-0" class="post not-found">
			<h1 class="post-title"><?php _e( 'Not Found', 'tie' ); ?></h1>
			<div class="entry">
				<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'tie' ); ?></p>
				<?php get_search_form(); ?>
			</div>
		</div>
		<?php endif; ?>

		<?php while ( have_posts() ) : the_post(); ?>
		<article <?php post_class('item-list'); ?>>
			<div class="post-format-icon">
				<a href="<?php the_permalink(); ?>" class="item-date"><span><?php the_time('j') ?></span><small><?php the_time('M') ?></small></a>
			</div>
			<div class="post-inner">
				<?php
					$format = get_post_format();
					if( false === $format ) { $format = 'standard'; }
				?>
				<?php get_template_part( 'content', $format ); ?>
				<?php edit_post_link( __( 'Edit', 'tie' ), '<span class="edit-link">', '</span>' ); ?>	
				
				<?php if( tie_get_option( 'post_tags' ) ) the_tags( '<p class="post-tag">'  ,' ', '</p>'); ?>
			</div><!-- .post-inner -->
		</article><!-- .post-listing -->
		
		<?php if( tie_get_option( 'share_post' ) ) : ?>			
		<div class="post-social">
			<span><?php _e( 'Share this Story' , 'tie' );?></span>
			<div>
				<a onClick="tie_share_post('http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>')" href="javascript:void(0);" rel="external"><i class="tieicon-facebook"></i></a>
				<a onClick="tie_share_post('http://twitter.com/home?status=<?php the_title(); ?> <?php the_permalink(); ?>')" href="javascript:void(0);"  rel="external" target="_blank"><i class="tieicon-twitter"></i></a>
				<a onClick="tie_share_post('https://plusone.google.com/_/+1/confirm?hl=en&amp;url=<?php the_permalink(); ?>&amp;name=<?php the_title(); ?>')" href="javascript:void(0);"  rel="external" target="_blank"><i class="tieicon-gplus"></i></a>
				<a onClick="tie_share_post('http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&amp;description=<?php the_title(); ?>&amp;media=<?php echo tie_full_image() ?>')" href="javascript:void(0);" rel="external" target="_blank"><i class="tieicon-pinterest-circled"></i></a>
				<a onClick="tie_share_post('http://linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>')" href="javascript:void(0);" rel="external" target="_blank"><i class="tieicon-linkedin"></i></a>
				<a onClick="tie_share_post('http://reddit.com/submit?url=<?php the_permalink(); ?>&title=<?php the_title(); ?>')" href="javascript:void(0);" rel="external" target="_blank"><i class="tieicon-reddit"></i></a>
				<a onClick="tie_share_post('mailto:?subject=<?php the_title(); ?>&body=<?php the_permalink(); ?>')" href="javascript:void(0);" rel="external" target="_blank"><i class="tieicon-mail-alt"></i></a>
			</div>
		</div>
		<?php endif; ?>
		
		<?php if( tie_get_option( 'post_nav' ) ): ?>				
		<div class="post-navigation">
			<div class="post-previous"><?php previous_post_link( '%link', '<span>'. __( 'Previous:', 'tie' ).'</span> %title' ); ?></div>
			<div class="post-next"><?php next_post_link( '%link', '<span>'. __( 'Next:', 'tie' ).'</span> %title' ); ?></div>
		</div><!-- .post-navigation -->
		<?php endif; ?>
		
		<?php if( tie_get_option( 'post_authorbio' ) ): ?>		
		<section id="author-box">
			<div class="block-head">
				<h3><?php _e( 'About', 'tie' ) ?> <?php the_author() ?> </h3>
			</div>
			<?php tie_author_box() ?>
		</section><!-- #author-box -->
		<?php endif; ?>
		<?php get_template_part( 'includes/post-related' ); // Get Related Posts template ?>	
		<?php endwhile;?>
		<?php comments_template( '', true ); ?>
	</div><!-- .content -->
	
<?php get_footer(); ?>