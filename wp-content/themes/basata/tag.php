<?php get_header(); ?>
	<div id="content">
		<div class="page-head">
			<h2 class="page-title">
				<?php printf( __( 'Tag: %s', 'tie' ), '<span>' . single_tag_title( '', false ) . '</span>' );	?>
			</h2>
		</div>
		<?php get_template_part( 'loop', 'tag' );	?>
		<?php if ($wp_query->max_num_pages > 1) tie_pagenavi(); ?>
	</div> <!-- .content -->
<?php get_footer(); ?>