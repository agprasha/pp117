<?php 
/*
Template Name: Sitemap Page
*/
?>
<?php get_header(); ?>
	<div id="content">
		<?php if ( ! have_posts() ) : ?>
		<div id="post-0" class="post not-found">
			<h1 class="post-title"><?php _e( 'Not Found', 'tie' ); ?></h1>
			<div class="entry">
				<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'tie' ); ?></p>
				<?php get_search_form(); ?>
			</div>
		</div>
		<?php endif; ?>
		<?php while ( have_posts() ) : the_post(); ?>
		<article <?php post_class('item-list'); ?>>
			<div class="post-inner">
				<h1 class="entry-title"><?php the_title(); ?></h1>
				<div class="entry">

					<?php the_content(); ?>
					
					<div id="sitemap">
						<div class="sitemap-col">
							<h2><?php _e('Pages','tie'); ?></h2>
							<ul id="sitemap-pages"><?php wp_list_pages('title_li='); ?></ul>
						</div> <!-- end .sitemap-col -->
							
						<div class="sitemap-col">
							<h2><?php _e('Categories','tie'); ?></h2>
							<ul id="sitemap-categories"><?php wp_list_categories('title_li='); ?></ul>
						</div> <!-- end .sitemap-col -->
							
						<div class="sitemap-col">
							<h2><?php _e('Tags','tie'); ?></h2>
							<ul id="sitemap-tags">
								<?php $tags = get_tags();
								if ($tags) {
									foreach ($tags as $tag) {
										echo '<li><a href="' . get_tag_link( $tag->term_id ) . '">' . $tag->name . '</a></li> ';
									}
								} ?>
							</ul>
						</div> <!-- end .sitemap-col -->
														
						<div class="sitemap-col<?php echo ' last'; ?>">
							<h2><?php _e('Authors','tie'); ?></h2>
							<ul id="sitemap-authors" ><?php wp_list_authors('optioncount=1&exclude_admin=0'); ?></ul>
						</div> <!-- end .sitemap-col -->
					</div> <!-- end #sitemap -->
				
					<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'tie' ), 'after' => '</div>' ) ); ?>
					
					<?php wp_reset_query(); ?>
					<?php edit_post_link( __( 'Edit', 'tie' ), '<span class="edit-link">', '</span>' ); ?>	
				</div>
			</div><!-- .post-inner -->
		</article><!-- .post-listing -->
		<?php endwhile;?>
		<?php comments_template( '', true ); ?>
	</div><!-- .content -->
<?php get_footer(); ?>